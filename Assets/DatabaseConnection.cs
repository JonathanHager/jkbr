﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Data;
using UnityNpgsql;
using Assets.manageaccount.src;
using Assets.PreGame.manageaccount.src;
using System.Text;
using System.Security.Cryptography;
using System;

public class DatabaseConnection
{

    public static DatabaseConnection Instance = new DatabaseConnection();


    private string connectedPlayer;
    public string ConnectedPlayer { get => connectedPlayer; }

    private SkinInfo equippedSkin;
    public SkinInfo EquippedSkin { get => equippedSkin; }

    private ServerInfo connectedToServer;
    public ServerInfo ConnectedToServer { get => connectedToServer; }

    string conString = "Server=192.168.178.52;Port=5432;" +
            "Database=jkbr;" +
            "User ID=Unity;" +
            "Password=secret;";

    public DatabaseConnection()
    {

    }

    #region Login
    public bool LogIn(string username, string password)
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected "+conn.Database);
            string passwordhash = Encoding.UTF8.GetString(GetHash(password));
            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"RobotSkin\".id, public.\"RobotSkin\".name, public.\"RobotSkin\".rarity FROM public.\"PlayerInfo\", public.\"RobotSkin\" WHERE public.\"PlayerInfo\".username = '" + username + "' AND public.\"PlayerInfo\".passwordhash = '" + passwordhash + "' and public.\"RobotSkin\".id = public.\"PlayerInfo\".equippedskin", conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                bool rows = reader.HasRows;
                // Debug.Log(rows);
                if (!rows)
                {
                    return false;
                }
                else
                {
                    reader.Read();
                    connectedPlayer = username;
                    this.equippedSkin = new SkinInfo((long)reader[0], (short)reader[2], (string)reader[1], "");
                    return true;
                }
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: "+e.Message);
                return false;
            }
        }


    }

    public static byte[] GetHash(string inputString)
    {
        HashAlgorithm algorithm = SHA256.Create();
        return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
    }

    public static string GetHashString(string inputString)
    {
        StringBuilder sb = new StringBuilder();
        foreach (byte b in GetHash(inputString))
            sb.Append(b.ToString("X2"));

        return sb.ToString();
    }

    #endregion

    #region Managment

    public long GetIntCoins()
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"PlayerInfo\".intcoins FROM public.\"PlayerInfo\" WHERE public.\"PlayerInfo\".username = '" + this.connectedPlayer+"'", conn);
            // Debug.Log(command.CommandText);
            try
            {
                int rows = command.ExecuteNonQuery();

                var reader = command.ExecuteReader();
                reader.Read();
                // Debug.Log("Ergebnis: " + reader[0]);
                return (long) reader[0];
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }
    }

    public List<SkinInfo> OwnedSkins()
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"RobotSkin\".id, public.\"RobotSkin\".name,public.\"RobotSkin\".rarity, public.\"Image\".path FROM public.\"RobotSkin\", public.\"player_owns_skin\", public.\"Image\" WHERE public.\"player_owns_skin\".playername = '" + this.connectedPlayer + "'" 
                + " and " + " public.\"player_owns_skin\".skinid = public.\"RobotSkin\".id and  public.\"Image\".id =  public.\"RobotSkin\".image", conn);

            try
            {
                var reader = command.ExecuteReader();
                reader.Read();
                List<SkinInfo> skins = new List<SkinInfo>();
                bool res = true;
                do
                {
                    long id = (long)reader[0];
                    string name = (string)reader[1];
                    int rarity = (int)((short)reader[2]);
                    string pathtoimage = (string)reader[3];

                    skins.Add(new SkinInfo(id, rarity, name, pathtoimage));
                    
                    res = reader.Read();
                } while (res);

                return skins;
            }
            catch (System.Exception)
            {
                throw;
            }finally
            {
                command.Dispose();
            }
        }
    }
    public List<SkinInfo> AllSkins()
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand("SELECT  public.\"RobotSkin\".id, public.\"RobotSkin\".name,public.\"RobotSkin\".rarity, public.\"Image\".path FROM public.\"RobotSkin\", public.\"player_owns_skin\", public.\"Image\" WHERE public.\"Image\".id =  public.\"RobotSkin\".image", conn);
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                List<SkinInfo> skins = new List<SkinInfo>();
                if(!reader.HasRows)
                {
                    return skins;
                }
                reader.Read();
                bool res = true;
                do
                {
                    long id = (long)reader[0];
                    string name = (string)reader[1];
                    int rarity = (int)((short)reader[2]);
                    string pathtoimage = (string)reader[3];

                    skins.Add(new SkinInfo(id, rarity, name, pathtoimage));

                    res = reader.Read();
                } while (res);

                return skins;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                command.Dispose();
            }
        }
    }

    public bool BuySkinIfPossible(SkinInfo skin)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"PlayerInfo\".intcoins FROM  public.\"PlayerInfo\" WHERE public.\"PlayerInfo\".username = '" + connectedPlayer+"'" , conn);
            // TODO: check if this works (res == number of rows)
            var reader = command.ExecuteReader();

            reader.Read();

            long coins = (long)reader[0] - skin.Rarity*1000;

            reader.Close();
            command.Dispose();

            if(coins<0)
            {
                return false;
            }

            command = new NpgsqlCommand("UPDATE public.\"PlayerInfo\" " +
                "SET intcoins = " + coins +" "+
                "WHERE public.\"PlayerInfo\".username = '"+connectedPlayer+"'", conn);

            command.ExecuteNonQuery();

            command.Dispose();

            command = new NpgsqlCommand("INSERT INTO public.\"player_owns_skin\" (playername, skinid) " +
                "VALUES ( '"+ connectedPlayer +"', "+ skin.Id +" )", conn);
            // Debug.Log(command.CommandText);
            command.ExecuteNonQuery();

            command.Dispose();



        }
        return true;
    }


    public void EquipSkin(SkinInfo skin)
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();


            NpgsqlCommand command = new NpgsqlCommand("UPDATE public.\"PlayerInfo\" SET equippedSkin="+skin.Id+
                " WHERE public.\"PlayerInfo\".username = '" + this.connectedPlayer +"'", conn);
            // Debug.Log(command.CommandText);
            command.ExecuteNonQuery();
            this.equippedSkin = skin;
            command.Dispose();
            
        }
    }
    #endregion

    #region FindGame

    public ServerInfo FindServer()
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Server\".id, public.\"Server\".hostname, public.\"Server\".portdata, public.\"Server\".portworld FROM public.\"Server\", public.\"Match\"" +
                " WHERE  public.\"Match\".started = False and public.\"Match\".server = public.\"Server\".id "
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                bool rows = reader.HasRows;
                // Debug.Log(rows);
                if (!rows)
                {
                    return null;
                }
                else
                {
                    reader.Read();

                    connectedToServer = new ServerInfo((long)reader[0], (string)reader[1], (int)(long)reader[2], (int)(long)reader[3]);

                    return connectedToServer;
                }
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }
    }

    #endregion

    #region Prefabs

    public string GetPathByPrefabId(int prefabid)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".path FROM public.\"Prefab\" WHERE id = "+prefabid+""
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }
    }

    public string GetWeaponNameById(int id)
    {
        try
        {
            using (NpgsqlConnection conn = new NpgsqlConnection(conString))
            {
                conn.Open();
                // Debug.Log("Connected " + conn.Database);

                NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Weapon\".name FROM public.\"Weapon\" WHERE id = " + id + ""
                    , conn);
                command.Connection = conn;
                // Debug.Log(command.CommandText);
                try
                {
                    var reader = command.ExecuteReader();
                    reader.Read();

                    return (string)reader[0];
                }
                catch (System.Exception e)
                {
                    // Debug.LogError("Exception: Database refused connection: " + e.Message);
                    return null;
                }
            }
        }catch(Exception e)
        {
            Debug.LogError(e.Message);
            return "";
        }

    }

    public string GetConsumableNameById(int id)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Consumable\".name FROM public.\"Consumable\" WHERE id = " + id + ""
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }

    }
    public string GetBuildItemNameById(int id)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"BuildItem\".name FROM public.\"BuildItem\" WHERE id = " + id + ""
                , conn);
            command.Connection = conn;
            
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();
                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }

    }
    #endregion

    #region id by names
    public long GetWeaponIdByName(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Weapon\".id FROM public.\"Weapon\" WHERE name = '" + weaponname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (long)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return -1;
            }
        }
    }
    public long GetConsumableIdByName(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Consumable\".id FROM public.\"Consumable\" WHERE name = '" + weaponname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (long)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return -1;
            }
        }
    }
    public long GetBuildItemIdByName(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"BuildItem\".id FROM public.\"BuildItem\" WHERE name = '" + weaponname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (long)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return -1;
            }
        }
    }
    #endregion

    #region Images

    // Returns the Path to an image for a weapon by its name
    public string ImagePathForWeapon(string weaponname)
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Image\".path FROM public.\"Weapon\", public.\"Image\" WHERE public.\"Weapon\".image = public.\"Image\".id and public.\"Weapon\".name = '" + weaponname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();
                Debug.Log("Weaponpath: " + reader[0]);
                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }
    }

    public string ImagePathForConsumable(string csname)
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Image\".path FROM public.\"Consumable\", public.\"Image\" WHERE public.\"Consumable\".image = public.\"Image\".id and public.\"Consumable\".name = '" + csname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }
    }

    public string ImagePathForBuildItem(string weaponname)
    {

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Image\".path FROM public.\"BuildItem\", public.\"Image\" WHERE public.\"BuildItem\".image = public.\"Image\".id and public.\"BuildItem\".name = '" + weaponname + "'"
                , conn);
            command.Connection = conn;
            // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                // Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }
    }
    #endregion
}
