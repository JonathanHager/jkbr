﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternVisualizer : MonoBehaviour
{
    int[][] pattern;
    // UI changes have to be done in main thread
    bool change = false;

    public int[][] Pattern { get => pattern; set { pattern = value; change = true; } }


    public Transform parent;

    public ParticleSystem prefab;

    ParticleSystem[][] particleSystems;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(change)
        {
            change = false;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    ParticleSystem ps = particleSystems[i][j];
                    var emmision = ps.emission;
                    emmision.rateOverTime = pattern[i][j];
                }
            }
        }
    }

    public void InstantiateParticleSystems()
    {
        particleSystems = new ParticleSystem[5][];
        for (int i = 0; i < 5; i++)
        {
            particleSystems[i] = new ParticleSystem[5];
            for (int j = 0; j < 5; j++)
            {
                particleSystems[i][j] = ParticleSystem.Instantiate(prefab, parent);
                particleSystems[i][j].transform.Translate((i - 2) * 10f, 0, (j - 2) * 10f);
            }
        }
    }
}
