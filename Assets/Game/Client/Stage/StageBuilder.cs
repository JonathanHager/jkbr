﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageBuilder : MonoBehaviour
{

    public GameObject groundPlane;
    public GameObject edgeParticles;
    // Start is called before the first frame update
    void Start()
    {
        BuildGroundPlanes();
    }

    // Update is called once per frame
    void Update()
    {}


    public void BuildGroundPlanes()
    {
        for (int i = 0; i < 30; i++)
        {
            for (int j = 0; j < 30; j++)
            {
                GameObject.Instantiate(groundPlane, new Vector3(i * 10+5, 0, j * (-10)-5), Quaternion.identity);
            }
        }
    }
}
