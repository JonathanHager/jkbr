﻿using Assets.Server.networking;
using Assets.Server.Warteschlange;
using Assets.Warteschlange;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientGameObjectManager : MonoBehaviour
{
    public ClientPrefabManager prefabManager;

    Dictionary<int, GameObject> gameObjects;

    // Do GUI things in Main Threads
    ChangeQueue queue = new ChangeQueue();

    // Start is called before the first frame update
    void Start()
    {
        this.gameObjects = new Dictionary<int, GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        while (queue.HasNext)
        {
            Change c = queue.GetFirst();
         //   Debug.Log("[CLIENTGOMANAGER] Verarbeite change " + c.ToString());
            switch (c.Type)
            {
                case 1:
                    this.InstantiateGameObject(c.Goid, c.Param1, c.Param2, c.Param3, (int)c.Param4);
                    break;
                case 2:
                    this.MoveGameObject(c.Goid, new Vector3(c.Param1, c.Param2, c.Param3));
                    break;
                case 3:
                    this.RotateGameObject(c.Goid, c.Param1, c.Param2, c.Param3, c.Param4);
                    break;
                case 4:
                    this.DestroyGameObject(c.Goid);
                    break;
                default:
                    break;
            }
        }
    }


    public void ReceiveChange(Change c)
    {
        queue.AddChange(c);
    }

    public void InstantiateGameObject(int id, float x, float y, float z, int prefabid)
    {
        //Debug.Log("[CLIENT] Spawne etwas prefabid=" + prefabid);
        GameObject prefab = prefabManager.GetPrefabById(prefabid);
        GameObject go = GameObject.Instantiate(prefab, new Vector3(x, y, z), Quaternion.identity);

        // Debug.Log("[CLIENT] Spawnte etwas prefabid=" + prefabid);
        this.gameObjects.Add(id, go);
    }

    public void MoveGameObject(int goid, Vector3 npos)
    {
        Debug.Log("[CLIENTGOMANAGER] bewege " + goid);
        GameObject go = this.gameObjects[goid];
        go.transform.position = npos;
    }

    public void RotateGameObject(int goid, float ax, float ay, float az, float degree)
    {
        GameObject go = this.gameObjects[goid];
        go.transform.Rotate(new Vector3(ax, ay, az), degree);
    }


    public void DestroyGameObject(int goid)
    {
        GameObject go = this.gameObjects[goid];
        GameObject.Destroy(go);
    }

    public void AnimateGameObject(int goid)
    {
        throw new NotImplementedException();
    }

    public GameObject GetGameObject(int id)
    {
        if (this.gameObjects.ContainsKey(id))
            return this.gameObjects[id];
        else
            return null;
    }
}
