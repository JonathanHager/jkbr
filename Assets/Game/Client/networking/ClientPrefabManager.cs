﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientPrefabManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() 
    {
        
    }

    internal string GetWeaponNameByItemId(int weaponSlot1)
    {
        return DatabaseConnection.Instance.GetWeaponNameById(weaponSlot1);
    }
    internal string GetBuildItemNameByItemId(int weaponSlot1)
    {
        return DatabaseConnection.Instance.GetBuildItemNameById(weaponSlot1);
    }
    internal string GetConsumableNameByItemId(int weaponSlot1)
    {
        return DatabaseConnection.Instance.GetConsumableNameById(weaponSlot1);
    }

    internal GameObject GetPrefabById(int prefabid)
    {
        return Resources.Load<GameObject>(DatabaseConnection.Instance.GetPathByPrefabId(prefabid));
    }
}
