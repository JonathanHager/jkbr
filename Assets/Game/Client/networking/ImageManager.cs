﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : MonoBehaviour
{
    public Sprite emptySprite;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public Sprite getSpriteByItemName(string itemname, byte type)
    {
        try
        {
            if (type == 1)
            {
                return Resources.Load<Sprite>(DatabaseConnection.Instance.ImagePathForWeapon(itemname));
            }
            if (type == 2)
            {
                return Resources.Load<Sprite>(DatabaseConnection.Instance.ImagePathForConsumable(itemname));
            }
            if (type == 3)
            {
                return Resources.Load<Sprite>(DatabaseConnection.Instance.ImagePathForBuildItem(itemname));
            }
        }
        catch (Exception)
        {
        }
        return emptySprite;

    }
}
