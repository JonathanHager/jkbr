﻿using Assets.Network;
using Assets.Server.networking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JKBRClient : MonoBehaviour
{

    public int DATA_SIZE = 5 * 1024;

    #region Declarations

    // Gameobjectmanager
    public ClientGameObjectManager cgomanager;

    // prefab manager
    public ClientPrefabManager prefabManager;

    // stuff, this, that
    TcpClient client;
    NetworkStream stuffstream;
    byte[] codebuffer;

    // World changes
    Socket worldsocket;
    NetworkStream worldStream;
    byte[] changeBuffer;


    // Login infos
    public GameObject loginCanvas;

    // Code infos
    public GameObject gameCanvas;
    public InputField codeInputField;
    public Button sendCodeButton;
    public Text codeInput;
    // Lock inputs if camera is moved => delock it only if no code is sent
    bool inputsLockedCodeSent = false;

    // Death screen
    public GameObject deathCanvas;

    // win screen
    public GameObject winnerwinnerjavadinnerCanvas;

    // 0 - login
    // 1 - ingame
    // 2 - death
    // 3 - winner
    private int state = 0;


    // Inventory images
    bool refreshInventory = false;
    public ImageManager imageManager;
    public Image weapon1Image;
    public Image weapon2Image;
    public Image consumableImage;
    public Image buildItemImage;
    // Inventory amount texts
    public Text consumableAmountText;
    public Text buildItemAmountText;

    // Sprites can only be set in Images in MainThread
    // => Saved in variable 
    // => Set in update methode to images
    private Sprite weapon1Sprite;
    private Sprite weapon2Sprite;
    private Sprite consumableSprite;
    private Sprite buildItemSprite;

    // inventory (current)
    private Inventory inventory;

    // Show damage (pattern)
    public PatternVisualizer patternVisualizer;
    bool showPrimary = false;
    int shownSlot;

    // Life
    public Slider lifeSlider;


    // Robot game object has to be marked so the player can see where he is
    private int robotgoid = -1;
    private bool isMarked = false;
    public GameObject mark;



    #endregion

    // Start is called before the first frame update
    void Start()
    {
        ConnectToServer();
    }

    // Update is called once per frame
    void Update()
    {

        #region Change between different UIs
        

        if (state == 1)
        {
            if (loginCanvas.activeInHierarchy)
            {
                loginCanvas.SetActive(false);
            }
            if (!gameCanvas.activeInHierarchy)
            {
                gameCanvas.SetActive(true);
            }
        }else if(state == 2)
        {
            if(gameCanvas.activeInHierarchy)
            {
                gameCanvas.SetActive(false);
            }
            if(!deathCanvas.activeInHierarchy)
            {
                deathCanvas.SetActive(true);
            }
        }
        else if (state == 3)
        {
            if (gameCanvas.activeInHierarchy)
            {
                gameCanvas.SetActive(false);
            }
            if (!winnerwinnerjavadinnerCanvas.activeInHierarchy)
            {
                winnerwinnerjavadinnerCanvas.SetActive(true);
            }
        }


        #endregion


        #region Set inventory images

        // Sprites to images
        if (refreshInventory)
        {
            try
            {
                Inventory i = inventory;
                // w
                weapon1Sprite = imageManager.getSpriteByItemName(this.prefabManager.GetWeaponNameByItemId(i.WeaponSlot1), 1);
                // w2
                weapon2Sprite = imageManager.getSpriteByItemName(this.prefabManager.GetWeaponNameByItemId(i.WeaponSlot2), 1);
                // c
                consumableSprite = imageManager.getSpriteByItemName(this.prefabManager.GetConsumableNameByItemId(i.ConsumableSlot1), 2);
                // bi
                buildItemSprite = imageManager.getSpriteByItemName(this.prefabManager.GetBuildItemNameByItemId(i.BuildItemSlot1), 3);
            }catch(Exception e)
            {
                Debug.LogError(e.Message);
            }

            if (weapon1Sprite != null)
            {
                this.weapon1Image.sprite = weapon1Sprite;
                weapon1Sprite = null;
            }
            if (weapon2Sprite != null)
            {
                this.weapon2Image.sprite = weapon2Sprite;
                weapon2Sprite = null;
            }
            if (consumableSprite != null)
            {
                this.consumableImage.sprite = consumableSprite;
                consumableSprite = null;
            }
            if (buildItemSprite != null)
            {
                this.buildItemImage.sprite = buildItemSprite;
                buildItemSprite = null;
            }

            consumableAmountText.text = "" + inventory.ConsumableSlot1Amount;
            buildItemAmountText.text = "" + inventory.BuildItemSlot1Amount;

            refreshInventory = false;
        }
        #endregion


        #region ActivateInputs

        if(!inputsLockedCodeSent && codeInputField.interactable==false)
        {

            codeInputField.interactable = true;
            sendCodeButton.interactable = true;
        }

        #endregion

        #region Things that should only appear on the robot which is controlled by this client

        // Mark robot
        if (!isMarked)
        {
            GameObject robotGameObject = this.cgomanager.GetGameObject(robotgoid);
            if (robotGameObject != null)
            {
                Debug.Log("Marking GameObject with id = "+robotgoid);
                GameObject go = GameObject.Instantiate(mark, robotGameObject.transform);
                isMarked = true;


                // Set robot transform to parent of particles
                this.patternVisualizer.parent = robotGameObject.transform;
                this.patternVisualizer.InstantiateParticleSystems();
            }

        }


        #endregion
    }

    #region Conenct to server


    private string hostname()
    {
        return DatabaseConnection.Instance.ConnectedToServer.Hostname;
    }

    private int dataport()
    {
        return DatabaseConnection.Instance.ConnectedToServer.Dataport;
    }

    private int worldport()
    {
        return DatabaseConnection.Instance.ConnectedToServer.Worldport;
    }

    private string username()
    {
        return DatabaseConnection.Instance.ConnectedPlayer;
    }


    public void ConnectToServer()
    {
        var host = hostname();
        var user = username();

        try
        {

            ConnectToServerForInventoryEtc(host, user);
            ConnectToServerWorldStuff(host);

        }
        catch(SocketException )
        {
            ShowConnectionFailed();
        }

    }

    void ShowConnectionFailed()
    {
        // TODO: Connection failed
    }

    #endregion


    #region Login for inventory, live etc
    public void ConnectToServerForInventoryEtc(string hostname, string playername)
    {
        client = new TcpClient();
        client.Connect(hostname, 32002);

        stuffstream = client.GetStream();

        codebuffer = new byte[DATA_SIZE];

        byte[] namebytes = System.Text.Encoding.UTF8.GetBytes(playername);

        // Write namebytes in codebuffer
        for (int i = 0; i < codebuffer.Length && i < namebytes.Length; i++)
        {
            codebuffer[i] = namebytes[i];
        }

        Debug.Log("[CLIENT] Verbinde mich");
        stuffstream.BeginWrite(codebuffer, 0, codebuffer.Length, WroteName, null);

    }

    void WroteName(IAsyncResult ar)
    {
        Debug.Log("[CLIENT] Name gesendet...");
        stuffstream.BeginRead(codebuffer, 0, 1, ReceivedLoginResult, null);
    }

    void ReceivedLoginResult(IAsyncResult ar)
    {
        if (codebuffer[0] == 0)
        {
            Debug.Log("[CLIENT] Login fehlgeschlagen");
            throw new Exception("Name already taken! :(");
        }
        else
        {

            stuffstream.BeginRead(codebuffer, 0, codebuffer.Length, StartGame, null);
        }
    }
    #endregion


    #region Game is running => Transfer inventory, code, lives etc
    void StartGame(IAsyncResult ar)
    {

        // ingame
        this.state = 1;

        robotgoid = BitConverter.ToInt32(codebuffer, 1);
        Debug.Log("RobotGO-ID: " + robotgoid);

        ActivateInputs();
        // TEST
        //SendCode();
        // ENDTEST
        Debug.Log("[CLIENT] Login erfolgreich");
    }

    string code()
    {
        return this.codeInputField.text+"\n";
    }


    public void SendCode()
    {
        string code = this.code();

        // clear code buffer
        codebuffer = new byte[codebuffer.Length];

        
        byte[] codebytes = System.Text.Encoding.UTF8.GetBytes(code);

        // Write codebytes in codebuffer
        for (int i = 0; i < codebuffer.Length && i < codebytes.Length; i++)
        {
            codebuffer[i] = codebytes[i];
        }

        stuffstream.BeginWrite(codebuffer, 0, codebuffer.Length, SentCode, null);

        Debug.Log("[CLIENT] Sende code " + code);

        inputsLockedCodeSent = true;
        codeInputField.interactable = false;
        sendCodeButton.interactable = false;
    }

    void SentCode(IAsyncResult ar)
    {
        stuffstream.BeginRead(codebuffer, 0, codebuffer.Length, ReceivedCodeResult, null);
    }

    void ReceivedCodeResult(IAsyncResult ar)
    {
        Debug.Log("RECEIVED " + codebuffer[0]);
        if (codebuffer[0] != 4)
        {
            // => Not inventory
            RoundOver();
        }
        else
        {
            try
            {
                Inventory i = Inventory.FromBytes(codebuffer);
                //    Debug.Log("Empfangenes Inventar: "+i.ToString());
                
                SetLifeDisplay(i.Life);

                inventory = i;

                refreshInventory = true;
            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
            }
            stuffstream.BeginRead(codebuffer, 0, codebuffer.Length, ReceivedCodeResult, null);
            //     Debug.Log("Empfangen abgeschlossen");
        }
    }

    // Sets the life stroke to the value
    void SetLifeDisplay(int life)
    {
        lifeSlider.value = life;
    }

    void RoundOver()
    {

        Debug.Log("[CLIENT] received " + codebuffer[0]);
        if (codebuffer[0] == 1)
        {
            ActivateInputs();
        }
        else if (codebuffer[0] == 2)
        {
            ShowDeathScreen();
        }
        else if (codebuffer[0] == 3)
        {
            ShowWinnerWinnerJavaDinner();
        }
    }


    void ActivateInputs()
    {
        inputsLockedCodeSent = false;
        Debug.Log("Activate Inputs");
    }
    #endregion

    #region Damage Indikator
    public void ToggleShowPrimary()
    {
        this.showPrimary = !this.showPrimary;
        ShowDamage(shownSlot);
    }

    public void ShowDamage(int weaponSlot)
    {
        shownSlot = weaponSlot;
        if(weaponSlot == 1)
        {
            javakarol.Weapon w = new javakarol.Weapon(Inventory.ItemNameById(inventory.WeaponSlot1, 1), 0, 0);

            this.patternVisualizer.Pattern = showPrimary ? w.pattern1.getPattern('N') : w.pattern2.getPattern('N');
        }
        else if (weaponSlot == 2)
        {
            javakarol.Weapon w = new javakarol.Weapon(Inventory.ItemNameById(inventory.WeaponSlot2, 1), 0, 0);

            this.patternVisualizer.Pattern = showPrimary ? w.pattern1.getPattern('N') : w.pattern2.getPattern('N');
        }
    }

    #endregion

    #region Inputs on/off for better camera controlling
    public void LockInputs()
    {
        codeInputField.interactable = false;
    }

    public void DelockInputs()
    {
        codeInputField.interactable = !inputsLockedCodeSent;
    }
    #endregion

    #region Show a screen after game is over
    void ShowDeathScreen()
    {
        // dead
        this.state = 2;
    }

    void ShowWinnerWinnerJavaDinner()
    {
        // winner
        this.state = 3;
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(1);
    }

    #endregion

    #region World Stream changes etc.

    public void ConnectToServerWorldStuff(string hostname)
    {
        changeBuffer = new byte[Change.BytesLength];

        worldsocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        worldsocket.Connect(hostname, 32001);
        worldStream = new NetworkStream(worldsocket);
     //   Debug.Log("[CLIENT] Verbunden mit Weltstream Check = " +
     //       worldsocket.Connected);
        worldsocket.BeginReceive(changeBuffer, 0, changeBuffer.Length, SocketFlags.None, ReceiveChange, null);
    }

    void ReceiveChange(IAsyncResult ac)
    {
      //  Debug.Log("[CLIENT] Empfange Änderung");
        Change c = Change.FromBytes(changeBuffer);
        HandleChange(c);

        worldStream.BeginRead(changeBuffer, 0, changeBuffer.Length, ReceiveChange, null);
    }

    void HandleChange(Change c)
    {
        cgomanager.ReceiveChange(c);
    }

    #endregion


    #region close sockets on destroy
    private void OnDestroy()
    {
        if (worldsocket == null)
            return;
        this.worldsocket.Close();
        this.stuffstream.Close();
    }

    #endregion
}
