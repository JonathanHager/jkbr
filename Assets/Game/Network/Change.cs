﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Server.networking
{
    public class Change
    {
        public static int BytesLength
        {
            get
            {
                return 2 * sizeof(int) + 5 * sizeof(float);
            }
        }

        int type;

        // Id of game object
        int goid;

        float p1 =0;
        float p2=0;
        float p3=0;
        float p4=0;
        float p5=0;

        public int Type { get => type;  }
        public int Goid { get => goid; }
        public float Param1 { get => p1; }
        public float Param2 { get => p2;  }
        public float Param3 { get => p3; }
        public float Param4 { get => p4;  }
        public float Param5 { get => p5; }

        public Change(int type, int goid)
        {
            this.type = type;
            this.goid = goid;
        }

        public Change(int type, int goid, float p1) : this(type, goid)
        {
            this.p1 = p1;
        }

        public Change(int type, int goid, float p1, float p2) : this(type, goid, p1)
        {
            this.p2 = p2;
        }

        public Change(int type, int goid, float p1, float p2, float p3) : this(type, goid, p1, p2)
        {
            this.p3 = p3;
        }

        public Change(int type, int goid, float p1, float p2, float p3, float p4) : this(type, goid, p1, p2, p3)
        {
            this.p4 = p4;
        }

        public Change(int type, int goid, float p1, float p2, float p3, float p4, float p5) : this(type, goid, p1, p2, p3, p4)
        {
            this.p5 = p5;
        }

        public byte[] ToBytes()
        {
            int byteslength = 2 * sizeof(int) + 5 * sizeof(float);
            byte[] bytes = new byte[byteslength];

            // single values
            byte[] typebytes = BitConverter.GetBytes(this.type);
            byte[] goidbytes = BitConverter.GetBytes(this.goid);
            byte[] p1bytes = BitConverter.GetBytes(this.Param1);
            byte[] p2bytes = BitConverter.GetBytes(this.Param2);
            byte[] p3bytes = BitConverter.GetBytes(this.Param3);
            byte[] p4bytes = BitConverter.GetBytes(this.Param4);
            byte[] p5bytes = BitConverter.GetBytes(this.Param5);

            int pointer = 0;
            foreach(byte b in typebytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in goidbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in p1bytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in p2bytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in p3bytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in p4bytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in p5bytes)
            {
                bytes[pointer++] = b;
            }

            return bytes;
        }

        public static Change FromBytes(byte[] bytes)
        {
            Change n = new Change(BitConverter.ToInt32(bytes, 0),
                BitConverter.ToInt32(bytes, 1 * sizeof(int)),
                BitConverter.ToSingle(bytes, 2 * sizeof(int) + 0 * sizeof(float)),
                BitConverter.ToSingle(bytes, 2 * sizeof(int) + 1 * sizeof(float)),
                BitConverter.ToSingle(bytes, 2 * sizeof(int) + 2 * sizeof(float)),
                BitConverter.ToSingle(bytes, 2 * sizeof(int) + 3 * sizeof(float)),
                BitConverter.ToSingle(bytes, 2 * sizeof(int) + 4 * sizeof(float)));

            return n;
        }

        public override string ToString()
        {
            return "[ goid="+this.goid+"; type="+this.type+"; " + this.p1 + "; " + this.p2 + "; " + this.p3 + "; " + this.p4 + "; " + this.p5 + "]";
        }
    }
}
