﻿using javakarol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Network
{
    class Inventory
    {


        int weaponSlot1 = -1;
        int weaponSlot2 = -1;

        int consumableSlot1 = -1;
        int consumableSlot1Amount = 0;

        int buildItemSlot1 = -1;
        int buildItemSlot1Amount = 0;

        // Inventory saves the life value to => Transfer is more simple
        int life = 100;

        public int WeaponSlot1 { get => weaponSlot1; set => weaponSlot1 = value; }
        public int WeaponSlot2 { get => weaponSlot2; set => weaponSlot2 = value; }
        public int ConsumableSlot1 { get => consumableSlot1; set => consumableSlot1 = value; }
        public int ConsumableSlot1Amount { get => consumableSlot1; set => consumableSlot1Amount = value; }
        public int BuildItemSlot1 { get => buildItemSlot1; set => buildItemSlot1 = value; }
        public int BuildItemSlot1Amount { get => buildItemSlot1Amount; set => buildItemSlot1Amount = value; }
        public int Life { get => life; set => life = value; }

        public Inventory(int w1, int w2, int c1, int c1a, int b1, int b1a, int life)
        {
            this.weaponSlot1 = w1;
            this.WeaponSlot2 = w2;
            this.ConsumableSlot1 = c1;
            this.BuildItemSlot1 = b1;
            this.life = life;
        }

        public static Inventory FromRobot(Robot r)
        {
            int ws1 = ItemId(r.getWeaponSlots()[0]);
            int ws2 = ItemId(r.getWeaponSlots()[1]);
            int c1 = ItemId(r.getConsumableSlots()[0]);
            int c1a = 0;
            if (r.getConsumableSlots()[0] != null)
            {
                c1a = r.getConsumableSlots()[0].getAmount();
            }
            
            int bi1 = ItemId(r.getBuildSlots()[0]);
            int bi1a = 0;
            if (r.getBuildSlots()[0] != null)
            {
                bi1a = r.getBuildSlots()[0].getAmount();
            }

            int life = r.getHealth();

            return new Inventory(ws1, ws2, c1, c1a, bi1, bi1a, life);
        }


        public static Inventory FromBytes(byte[] bytes)
        {
            return new Inventory(
                BitConverter.ToInt32(bytes, sizeof(byte)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 1 *sizeof(int)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 2 * sizeof(int)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 3 * sizeof(int)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 4* sizeof(int)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 5 * sizeof(int)),
                BitConverter.ToInt32(bytes, sizeof(byte) + 6 * sizeof(int))
                );
        }


        public byte[] ToBytes()
        {
            int byteslength = JKBRServer.DATA_SIZE;
            byte[] bytes = new byte[byteslength];

            // single values
            byte[] weaponslotbytes = BitConverter.GetBytes(weaponSlot1);
            byte[] weaponslot2bytes = BitConverter.GetBytes(WeaponSlot2);
            byte[] consumableslotbytes = BitConverter.GetBytes(ConsumableSlot1);
            byte[] cunsumableslotamountbytes = BitConverter.GetBytes(ConsumableSlot1Amount);
            byte[] builditemslotbytes = BitConverter.GetBytes(BuildItemSlot1);
            byte[] builditemslotamountbytes = BitConverter.GetBytes(BuildItemSlot1Amount);
            byte[] lifebytes = BitConverter.GetBytes(Life);

            // tell client its a inventory
            bytes[0] = 4;
            int pointer = 1;
            foreach (byte b in weaponslotbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in weaponslot2bytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in consumableslotbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in cunsumableslotamountbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in builditemslotbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in builditemslotamountbytes)
            {
                bytes[pointer++] = b;
            }
            foreach (byte b in lifebytes)
            {
                bytes[pointer++] = b;
            }

            return bytes;
        }
        

        // Clientside only
        public static int ItemId(Item i)
        {
            if (typeof(Weapon).IsInstanceOfType(i))
            {
                return (int)DatabaseConnection.Instance.GetWeaponIdByName(i.getName());
            }
            if (typeof(Consumable).IsInstanceOfType(i))
            {
                return (int)DatabaseConnection.Instance.GetConsumableIdByName(i.getName());
            }
            if (typeof(BuildItem).IsInstanceOfType(i))
            {
                return (int)DatabaseConnection.Instance.GetBuildItemIdByName(i.getName());
            }
            return -1;
        }

        public static string ItemNameById(int id, byte type)
        {
            switch (type)
            {
                case 1:
                    return DatabaseConnection.Instance.GetWeaponNameById(id);
                case 2:
                    return DatabaseConnection.Instance.GetConsumableNameById(id);
                case 3:
                    return DatabaseConnection.Instance.GetBuildItemNameById(id);
                default:
                    break;
            }
            return "unknown";
        }

        public override string ToString()
        {
            return "[ " + WeaponSlot1 + " ] [" + WeaponSlot2 + " ] [" + consumableSlot1 + ": " + ConsumableSlot1Amount +" ] [" + buildItemSlot1 + ": " +BuildItemSlot1Amount + " ] [ " + life + " ]";
        }

    }
}
