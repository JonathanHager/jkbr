﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;

namespace codecontrol
{
    class CodeControl
    {
        String[] code;

        public CodeControl(String[] code)
        {
            this.code = code;
        }

        public Command compile()
        {
            Command command = new CommandEmpty();
            foreach (var exe in Converter.convert(code))
            {

                exe.execute(command);
            }

            return command;
        }

        /*
         * Currently a nice way of testing!
         */
        public static void Main(String[] args)
        {
            String[] test = { "step();", "if(isWall()){", "step();", "}" };
            CodeControl cc = new CodeControl(test);
            Command c = cc.compile();
        }
    }
}
