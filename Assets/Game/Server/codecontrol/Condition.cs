﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;
using javakarol;

namespace codecontrol
{
    class Condition
    {

        String condition;

        public Condition(String condition)
        {
            this.condition = condition;
        }

        public bool isTrue(Robot r)
        {
            if (condition.StartsWith("!"))
            {
                condition = condition.Substring(1);
                //			System.out.println( ! checkCondition(r));
                return !checkCondition(r);
            }
            return checkCondition(r); //Not that u would ever reach this part?!?!
        }

        private bool checkCondition(Robot r)
        {
            switch (condition)
            {
                case "isWall()":
                    return r.isBorder(); //robot.isWall();
                case "isBlock()":
                    return r.isBlock(); //robot.isBlock();
                case "isRobot()":
                    return r.isRobotInFrontOfMe();
                case "slot[0].isRobotInAttackRange(1)":
                    return r.isRobotInAttackRange(0, true);
                case "slot[0].isRobotInAttackRange(2)":
                    return r.isRobotInAttackRange(0, false);
                case "slot[1].isRobotInAttackRange(1)":
                    return r.isRobotInAttackRange(1, true);
                case "slot[1].isRobotInAttackRange(2)":
                    return r.isRobotInAttackRange(1, false);
                    //Throw Error!

            }
            return false;
        }
    }
}
