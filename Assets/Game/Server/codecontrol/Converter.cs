﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codecontrol
{
    class Converter
    {
        public static LinkedList<Executable> convert(String[] code)
        {

            LinkedList<Executable> executables = new LinkedList<Executable>();

            for (int i = 0; i < code.Length; i++)
            {
                if (code[i].EndsWith(";"))
                {
                    executables.AddLast(new ExecutableCode(code[i].Substring(0, code[i].Length - 1)));
                }
                else if (code[i].EndsWith("{"))
                {
                    int bracketCount = 1;
                    for (int j = i + 1; j < code.Length; j++)
                    {

                        if (code[j].EndsWith("{"))
                        {
                            bracketCount++;
                        }
                        else if (code[j].Equals("}"))
                        {
                            bracketCount--;
                        }

                        if (bracketCount == 0)
                        {

                            if (code[i].StartsWith("if"))
                            {
                                if (code[i + 1].StartsWith("else"))
                                {
                                    int bracketCountElse = 1;
                                    for (int m = j + 1; m < code.Length; m++)
                                    {
                                        if (code[m].EndsWith("{"))
                                        {
                                            bracketCountElse++;
                                        }
                                        else if (code[m].Equals("}"))
                                        {
                                            bracketCountElse--;
                                        }
                                        if (bracketCountElse == 0)
                                        {

                                            executables.AddLast(new ExecutableIf(Utils.getParameter(code[i]), copyOfRange(code, i + 1, j), copyOfRange(code, j + 1, m)));
                                        }
                                    }
                                }
                                else
                                {
                                    executables.AddLast(new ExecutableIf(Utils.getParameter(code[i]), copyOfRange(code, i + 1, j)));
                                }
                            }
                            else if (code[i].StartsWith("while"))
                            {
                                executables.AddLast(new ExecutableWhile(Utils.getParameter(code[i]),
                                        copyOfRange(code, i + 1, j)));
                            }
                            else if (code[i].StartsWith("for"))
                            {
                                executables.AddLast(new ExecutableFor(copyOfRange(code, i + 1, j),
                                        Utils.forLoop(code[i]).getVariableName(), Utils.forLoop(code[i]).getInitial(),
                                        Utils.forLoop(code[i]).getEnd(), Utils.forLoop(code[i]).getStep()));
                            }

                            i = j;
                            break;
                        }

                    }
                }
            }
            return executables;
        }


        static String[] copyOfRange(String[] code, int from, int to)
        {
            int length = to - from;
            String[] n = new string[length];
            Array.Copy(code, from, n, 0, length);
            return n;
        }
    }
}
