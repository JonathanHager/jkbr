﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;

namespace codecontrol
{
    public interface Executable
    {
        void execute(Command command);
    }
}
