﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;
using javakarol;
using UnityEngine;

namespace codecontrol
{
    class ExecutableCode : Executable
    {
        String code;

        public ExecutableCode(String code)
        {
            this.code = code;
        }

        void Executable.execute(Command c)
        {
            switch (code)
            {
                case "step()":
                    Debug.Log("Step will be added");
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.forward();
                    }));
                    break;
                case "slot[0].pick()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {

                        r.pickUp(0);
                    }));
                    break;
                case "slot[1].pick()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {

                        r.pickUp(1);


                    }));
                    break;
                case "slot[2].pick()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.pickUp(2);


                    }));
                    break;
                case "slot[3].pick()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.pickUp(3);


                    }));
                    break;
                case "slot[0].attack(1)":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.attack(0, true);


                    }));
                    break;
                case "slot[0].attack(2)":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.attack(0, false);


                    }));
                    break;
                case "slot[1].attack(1)":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.attack(1, true);


                    }));
                    break;
                case "slot[1].attack(2)":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.attack(1, false);


                    }));
                    break;
                case "slot[2].consume()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.consume(0);

                    }));
                    break;
                case "slot[3].place()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.placeBlock(0);


                    }));
                    break;
                case "left()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.left();


                    }));
                    break;
                case "right()":
                    c.addCommand(new CommandActionExpression((r) =>
                    {
                        r.right();

                    }
                    ));
                    break;
                default:
                    c.addCommand(new CommandError(code));
                    break;
            }
        }
    }
}
