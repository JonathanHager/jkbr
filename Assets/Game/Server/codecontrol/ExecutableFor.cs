﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;

namespace codecontrol
{
    class ExecutableFor : Executable
    {

        String variableName;
        int start;
        int end;
        int stepWidth;
        String[] code;

        public ExecutableFor(String[] code, String variableName, int start, int end, int stepWidth)
        {
            this.code = code;
            this.variableName = variableName;
            this.start = start;
            this.end = end;
            this.stepWidth = stepWidth;
        }
        
        void Executable.execute(Command c)
        {
            LinkedList<Executable> executable = Converter.convert(code);

            Command s = new CommandEmpty();

            for (int i = start; i < end; i += stepWidth)
            {
                foreach (var exe in executable)
                {
                    exe.execute(s);
                }
        }

        c.addCommand(s);
		
	}
        
    }
}
