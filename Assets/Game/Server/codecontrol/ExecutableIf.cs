﻿using codecontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;

namespace codecontrol
{
    class ExecutableIf : Executable
    {

        String condition;
        String[] codeIf;
        String[] codeElse;

        public ExecutableIf(String condition, String[] codeIf, String[] codeElse)
        {
            this.condition = condition;
            this.codeIf = codeIf;
            this.codeElse = codeElse;
        }

        public ExecutableIf(String condition, String[] codeIf)
        {
            this.condition = condition;
            this.codeIf = codeIf;
        }


        // Only the first command in if can be executed
            void Executable.execute(Command c)
        {
            LinkedList<Executable> executableIf = Converter.convert(codeIf);
            if (codeElse != null)
            {
                LinkedList<Executable> executableElse = Converter.convert(codeElse);

                CommandCondition n = new CommandConditionExpression((robot)=>    
                {
                    Condition cond = new Condition(condition);
                    return cond.isTrue(robot);
                }
            );
            Command te = new CommandEmpty();
                foreach (var exe in executableIf)
                {
                    exe.execute(te);

                }
            n.setNextCommandTrue(te);
            Command f = new CommandEmpty();
                foreach (var exe in executableElse)
                {

                    exe.execute(f);
                }
            n.setNextCommandFalse(f);

            c.addCommand(n);
        }else {
			
			CommandCondition n = new CommandConditionExpression((robot)=>
        {
            Condition co = new Condition(condition);
            return co.isTrue(robot);
        }
    );
    Command t = new CommandEmpty();
                foreach (var exe in executableIf)
                {
                    exe.execute(t);

                }
			n.setNextCommandTrue(t);
			
			c.addCommand(n);
		}
	}

    }
}
