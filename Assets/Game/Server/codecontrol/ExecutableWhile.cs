﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandSystem;
using javakarol;

namespace codecontrol
{
    class ExecutableWhile : Executable
    {
        String condition;
        String[] code;

        public ExecutableWhile(String conditon, String[] code)
        {
            this.condition = conditon;
            this.code = code;
        }


        void Executable.execute(Command c)
        {
            LinkedList<Executable> executable = Converter.convert(code);

            CommandWhile cond = new CommandWhileExpression((robot) =>
            {
                Condition co = new Condition(condition);
                return co.isTrue(robot);
            }
        ) ;

        Command com = new CommandEmpty();
            foreach (var exe in executable)
            {
                exe.execute(com);

            }
		com.addCommand(cond);
		cond.setNextCommandTrue(com);
		
		c.addCommand(cond);
	
	}

    }
}
