﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace codecontrol
{
    public class LoopConditions
    {
        String variableName;
        int initial;
        int end;
        int step;

        /**
         * 
         * @param variableName The name of the variable that is used in the for loop
         * @param initial The value with which the variable is initialized 
         * @param end The end or final value of the variable
         * @param step How much the variable is increased on every iteration, e.g. 1 -> += 1
         */

        public LoopConditions(String variableName, int initial, int end, int step)
        {
            this.variableName = variableName;
            this.initial = initial;
            this.end = end;
            this.step = step;
        }

        public String getVariableName()
        {
            return variableName;
        }

        public void setVariableName(String variableName)
        {
            this.variableName = variableName;
        }

        public int getInitial()
        {
            return initial;
        }

        public void setInitial(int initial)
        {
            this.initial = initial;
        }

        public int getEnd()
        {
            return end;
        }

        public void setEnd(int end)
        {
            this.end = end;
        }

        public int getStep()
        {
            return step;
        }

        public void setStep(int step)
        {
            this.step = step;
        }
    }

    class Utils
    {
       



        /**
         * Method to handle regex in one line
         * 
         * @param input The input String from which the subString will be created
         * @param regex A regex pattern that results in group(s)
         * @return Depending on the amount of groups this will have the corresponding
         *         length
         */

        public static String[] subString(String input, String regex)
        {
            
            Match m = Regex.Match(input, regex);
            
            String[] output = new String[m.Groups.Count];

            if (m.Success)
            {
                for (int i = 0; i < m.Groups.Count; i++)
                {
                    output[i] = m.Groups[i].ToString() ;
                }
                return output;
            }
            else
            {
                return null;
            }
        }

        public static String getParameter(String command)
        {
            return Utils.subString(command, ".+?\\((.*)\\)\\s*(;|(?:\\{(?:\\s*\\})?)?)\\s*$")[1];
        }

        public static LoopConditions forLoop(String command)
        {

            String[] parts = Utils.subString(command, ".+?\\((.*)\\)\\s*(;|(?:\\{(?:\\s*\\})?)?)\\s*$")[1].Split(';');
            String variableName = parts[0].Split('=')[0];
            int start = int.Parse(parts[0].Split('=')[1].Trim());
            int stop = int.Parse(parts[1].Trim());
            int step = int.Parse(parts[2].Trim());

            return new LoopConditions(variableName, start, stop, step);
        }
    }
}
