﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
     public abstract class Command
    {// A command is one step a robot can execute
        public abstract Command execute(Robot robot) ;

        // Recursive structure
        public abstract void addCommand(Command c);
    }
}
