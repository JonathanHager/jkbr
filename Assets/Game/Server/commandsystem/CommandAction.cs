﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    abstract class CommandAction :Command
    {
        private Command nextCommand;

        // Setter
        public void setNextCommand(Command nextCommand)
        {
            this.nextCommand = nextCommand;
        }

        
        public override Command execute(Robot robot)
        {
            //		System.out.print(" CODE; ->");
            code(robot);
            // returns next command which should be executed
            return nextCommand;
        }

        // Executes code (things that takes a step);
        public abstract void code(Robot robot);


        // Recursion
     
    public override void addCommand(Command c)
        {
            if (nextCommand == null)
            {
                nextCommand = c;
                return;
            }
            nextCommand.addCommand(c);
        }
    }
}
