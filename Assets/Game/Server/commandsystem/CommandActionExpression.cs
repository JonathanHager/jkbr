﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    class CommandActionExpression : CommandAction
    {

        Action<Robot> codeExecution;

        public CommandActionExpression(Action<Robot> action)
        {
            this.codeExecution = action;
        }

        public override void code(Robot robot)
        {
            codeExecution(robot);
        }
    }
}
