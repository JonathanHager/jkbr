﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    abstract class CommandCondition : Command
    {

        protected Command nextCommandTrue;
        protected Command nextCommandFalse;

        public CommandCondition()
        {
        }

        public void setNextCommandTrue(Command nextCommandTrue)
        {
            this.nextCommandTrue = nextCommandTrue;
        }

        public void setNextCommandFalse(Command nextCommandFalse)
        {
            this.nextCommandFalse = nextCommandFalse;
        }

        public override Command execute(Robot robot)
        {
            // Returns the command which should be next
            if (condition(robot))
            {
                //			System.out.print(" TRUE ->");
                if (nextCommandTrue == null)
                {
                    return null;
                }
                // It takes no time, so it executes the next command in at the same step
                return nextCommandTrue.execute(robot);
            }
            else
            {
                //			System.out.print(" FALSE ->");
                if (nextCommandFalse == null)
                {
                    return null;
                }
                return nextCommandFalse.execute(robot);
            }
        }

        public abstract bool condition(Robot robot);

        // Recursion
        public override void addCommand(Command c)
        {
            if (nextCommandTrue == null)
            {
                nextCommandTrue = c;
            }
            else
            {
                nextCommandTrue.addCommand(c);
            }
            if (nextCommandFalse == null)
            {
                nextCommandFalse = c;
            }
            else
            {
                nextCommandFalse.addCommand(c);
            }
        }
    }
}
