﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    class CommandConditionExpression : CommandCondition
    {

        Func<Robot, bool> action;

        public CommandConditionExpression(Func<Robot, bool> a)
        {
            this.action = a;
        }

        public override bool condition(Robot robot)
        {
            return action(robot);
        }
    }
}
