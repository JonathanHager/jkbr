﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    class CommandEmpty : Command
    {
        private Command nextCommand;

        // Command which is overjumped (only needed for having a first element)
    public override Command execute(Robot robot) 
        {
            

		if(nextCommand==null)
		{
			return null;
		}
		return nextCommand.execute(robot);
	}
    
    public override void addCommand(Command c)
    {
        if (nextCommand == null)
        {
            nextCommand = c;
            return;
        }
        nextCommand.addCommand(c);
    }
}
}
