﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    class CommandError:Command
    {
        private Command nextCommand;
        private String line;

        public CommandError(String s)
        {
            line = s;
        }

        // Throws error if code is invalid
    public override Command execute(Robot robot) 
        {
            return nextCommand;
		//throw new Exception(line);
    }
        
    public override void addCommand(Command c)
    {
        if (nextCommand == null)
        {
            nextCommand = c;
        }
        else
        {
            nextCommand.addCommand(c);
        }
    }
}
}
