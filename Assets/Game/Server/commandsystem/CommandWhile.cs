﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommandSystem
{
    abstract class CommandWhile : CommandCondition
    {
        // Quite similar to a condition
    public override void addCommand(Command c)
        {
            if (nextCommandFalse == null)
            {
                nextCommandFalse = c;
            }
            else
            {
                nextCommandFalse.addCommand(c);
            }
        }
    }
}
