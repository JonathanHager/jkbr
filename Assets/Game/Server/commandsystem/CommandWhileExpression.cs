﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using javakarol;

namespace CommandSystem
{
    class CommandWhileExpression : CommandWhile
    {

        Func<Robot, bool> action;

        public CommandWhileExpression(Func<Robot, bool> a)
        {
            action = a;
        }

        public override bool condition(Robot robot)
        {
            return action(robot);
        }
    }
}
