﻿using Assets.Network;
using Assets.Server.game;
using Assets.Server.networking;
using Assets.Server.Warteschlange;
using Assets.Warteschlange;
using codecontrol;
using CommandSystem;
using javakarol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // TODO: have to be taken from the database
    public List<string> WeaponNames = new List<string>()
    {
        "sword",
        "axe"
    };
    public List<string> ConsumableNames = new List<string>()
    {
        "apple"
    };
    public List<string> BuildItemNames = new List<string>()
    {
        "item_trap_batman_hat"
    };
    // ENDTODO

    public int PlayerCount = 1;

    public static GameManager Instance;

    public RoundSimulator roundSimulator;

    public GameObjectManager gameObjectManager;

    public PrefabManager prefabManager;
    
    // List of all players (Player holds javakarol.Robot and the GameObject which represents the Robot)
    List<Player> players = new List<Player>();
    // Attributes whicht tells the Server that he should keep accepting new players
    public bool AcceptNewPlayers
    {
        get
        {
            Debug.Log("Aktuelle Anzahl: " + players.Count);
            return this.players.Count < PlayerCount;
        }
    }

    // Saves a list with all players ever were connected to the game
    List<string> allPlayers = new List<string>();


    // Dictionary which saves the GameObjects id by a Item
    Dictionary<Item, int> itemGameObjects;

    // Dictionary which saves the GO-ID of a block
    List<int> blockGameObjects;

    // Saves which players still need to send some code
    Dictionary<string, bool> playerNameSentCode;
    private bool startRound
    {
        get
        {
            foreach (var item in this.playerNameSentCode.Keys)
            {
                if (!this.playerNameSentCode[item])
                {
                    return false;
                }
            }
            return true;
        }
    }


    // Some operations like spawning have to be executed in the main thread
    Warteschlange queue = new Warteschlange();


    // Start is called before the first frame update
    void Start()
    {
        Instance = this;


        this.players = new List<Player>();
        this.itemGameObjects = new Dictionary<Item, int>();
        this.blockGameObjects = new List< int>();
        this.playerNameSentCode = new Dictionary<string, bool>();

    }

    // Update is called once per frame
    void Update()
    {

        // Executes the Spawning operations in the main thread
        while(queue.HasNext)
        {
            queue.GetNextElement().Execute(this);
        }
    }


    #region  Registrating players

    // Check if name is available 
    // Because of the database connection two players shouldn't have the same name
    // so it checks more or less if a player is already conected
    public bool IsNameAvailable(string name)
    {
       // Debug.Log("Hi");
        foreach (var player in this.players)
        {
            if (player.Name.Equals(name))
            {
                return false;
            }
        }
//        Debug.Log("[SERVER-GAMEMANAGER] " + name + " ist verfügbar");
        return true;
    }

    // Adds a player to the player-Lists
    // Adds a Robot to the model (javakarol-package)
    // Does not already spawn a GameObject (GameObect is spawned when all players are connected)
    public void RegisterPlayer(Player nplayer)
    {
        nplayer.JKBRRobot = CreateJKBRRobot(nplayer);
        
        this.playerNameSentCode.Add(nplayer.Name, false);

        this.players.Add(nplayer);
        this.allPlayers.Add(nplayer.Name);
    }

    // Returns x-, y-Position and the orientation of the next Robot in an int array
    private int[] nextPositions()
    {
        System.Random r = new System.Random();

        int x = r.Next(Map.map.getElements().Length);
        int y = r.Next(Map.map.getElements()[x].Length);


        while(distanceSquareToNextRobot(x,y) < 5*5)
        {
            x = r.Next(Map.map.getElements().Length);
            y = r.Next(Map.map.getElements()[x].Length);
        }
        
        return new int[] { x, y, 'S' };
    }

    // Calculates distance to next Robot (relevant for spawning position)
    private int distanceSquareToNextRobot(int x, int y)
    {
        int minDistance = int.MaxValue;

        foreach(var player in this.players)
        {
            int distanceSquare = (x - player.JKBRRobot.getX()) * (x - player.JKBRRobot.getX()) + (y - player.JKBRRobot.getY()) * (y - player.JKBRRobot.getY());
            minDistance = Math.Min(minDistance, distanceSquare);
        }

        return minDistance;
    }

    // Creates a JKBR Robot
    public Robot CreateJKBRRobot(Player p)
    {
        int[] pos = this.nextPositions();
        Robot nrobot = new Robot(pos[0], pos[1], p.Name, (char)pos[2]);
        nrobot.addBuildItemUseListener(JKBRListener.Instance);
        nrobot.addConsumeListener(JKBRListener.Instance);
        nrobot.addDeathListener(JKBRListener.Instance);
        nrobot.addItemPickedUpListener(JKBRListener.Instance);
        nrobot.addRotationListener(JKBRListener.Instance);
        nrobot.addStepListener(JKBRListener.Instance);
        nrobot.addWeaponUseListener(JKBRListener.Instance);
        return nrobot;
    }

    // creates a game object 
    // Warning: Requires a JKBRRobot in the player-object
    public GameObject CreateRobotGameObject(Player p)
    {
       // Debug.Log("Spawne echt roboter");
        GameObject prefab = prefabManager.GetRobotPrefab(p);
       // Debug.Log("Jetzt");
        try
        {
            GameObject ngameobject = GameObject.Instantiate(prefab, JKBRCordToUnity(p.JKBRRobot.getX(), p.JKBRRobot.getY(),
                Map.map.getElements()[p.JKBRRobot.getX()][p.JKBRRobot.getY()].height())
                , Quaternion.identity);

           // Debug.Log("Spawnte echt roboter");
            return ngameobject;
        }catch(Exception e)
        {
            Debug.Log(e.Message);
        }
        return null;
    }

    #endregion

    #region Items GameObjects

    // Start Game (Adds the GameObjects which has to be spawned to the Queue)
    public void StartGame()
    {
        Debug.Log("[GAMEMANAGER] Starte...");
        // Spawn robots
        foreach (var item in this.players)
        {
         //   Debug.Log("[GAMEMANAGER] Spawne Roboter " + item.Name);
            queue.AddPlayerGameObject(item);
        //    Debug.Log("[GAMEMANAGER] Ist gespawnet");
        }

        SpawnItemsOnMap();


    }

    // Method which generates the items on the map
    void SpawnItemsOnMap()
    {
        System.Random random = new System.Random();
        // TEst
        SpawnWeapon("sword", 0, 1);
        // Weapons
        for (int i = 0; i < this.players.Count*3; i++)
        {
            SpawnWeapon(this.WeaponNames[random.Next(this.WeaponNames.Count)], random.Next(30), random.Next(30));
        }
        // Weapons
        for (int i = 0; i < this.players.Count * 2; i++)
        {
            SpawnConsumable(this.ConsumableNames[random.Next(this.ConsumableNames.Count)], random.Next(30), random.Next(30));
        }
        // Weapons
        for (int i = 0; i < this.players.Count * 3; i++)
        {
            SpawnBuildItem(this.BuildItemNames[random.Next(this.BuildItemNames.Count)], random.Next(30), random.Next(30));
        }
    }

    // Spawn item in model (javakarol) and view
    public void SpawnWeapon(string name, int x, int y)
    {
        // Model
        Weapon w = new Weapon(name, x, y);

        queue.AddItemGameObject(w, x, y);

        //Debug.Log("Spawne " + name);
    }

    public void SpawnConsumable(string name, int x, int y)
    {
        // Model
        Consumable c = new Consumable(name, x, y);

        queue.AddItemGameObject(c, x, y);
    }

    public void SpawnBuildItem(string name, int x, int y)
    {
        // Model
        BuildItem b = new BuildItem(name, x, y);

        queue.AddItemGameObject(b, x, y);

        //Debug.Log("Spawne " + name);
    }

    public void SpawnItemGameObject(Item i,int x,int y)
    {
        //dDebug.Log("Spawne " + i.getName() + " prefabid=" + prefabManager.GetPrefabIdOfItem(i.getName()));
        // View
        GameObject prefab = null ;
        int prefabid = -1;
        if(typeof(Weapon).IsInstanceOfType(i))
        {
            prefab = prefabManager.GetWeaponPrefab(i.getName());
            prefabid = prefabManager.GetPrefabIdOfWeapon(i.getName());
           // Debug.Log("WeaponPrefab: "+ prefab + " id=" + prefabid);
        }
        else
        if (typeof(Consumable).IsInstanceOfType(i))
        {
            prefab = prefabManager.GetConsumablePrefab(i.getName());
            prefabid = prefabManager.GetPrefabIdOfConsumable(i.getName());
           // Debug.Log("ConsumablePrefab: " + prefab + " id=" + prefabid);
        }else
        if (typeof(BuildItem).IsInstanceOfType(i))
        {
            prefab = prefabManager.GetBuildItemPrefab(i.getName());
            prefabid = prefabManager.GetPrefabIdOfBuildItem(i.getName());
          //  Debug.Log("BuildItemPrefab: " + prefab + " id=" + prefabid);
        }
        else
        {
           // Debug.Log("Was???");
        }
        GameObject go = GameObject.Instantiate(prefab, JKBRCordToUnity(x, y, Map.map.getElements()[x][y].height()), Quaternion.identity);
        int goid = gameObjectManager.RegisterGameObject(go, prefabid);

        // Save
        this.itemGameObjects.Add(i, goid);
    }


    // Remove Item when picked up
    public void ItemPickedUp(Item i)
    {
        int goid = this.itemGameObjects[i];
        gameObjectManager.AddChange(new Change(4, goid));
    }
    #endregion

    #region Receiving Code


    public void ReceiveCode(string playername, string code)
    {
        code.Replace("\0", "");
        string[] lines = code.Split('\n');
        Command command = (new CodeControl(lines)).compile();
        Player p = playerByName(playername);
        this.playerNameSentCode[p.Name] = true;
        p.SetCommand(command);

     //   Debug.Log(code);
        if(startRound)
        {
            roundSimulator.StartRound();
        }
    }

    #endregion

    #region Just like the simulations!

    public void ExecuteStep()
    {
        // IDEA: Change the order of the players to control whose code is executed at first
        foreach (var player in this.players)
        {
         //   Debug.Log("[GAMEMANAGER] " + player.Name + ", Mach was!");
            player.DoStep();
        }
    }

    // Move the gameobject of a robot
    public void RobotMoves(Robot r, int nx, int ny)
    {
        Player p = playerByRobot(r);

        Vector3 pos = JKBRCordToUnity(nx, ny, Map.map.getElements()[nx][ny].height());
        gameObjectManager.AddChange(new Change(2, p.RobotObjectID, pos.x, pos.y, pos.z));
    }


    public void BlockPlaced(Block b, int x, int y)
    {
        queue.AddBlockGameObject(b, x, y);
    }

    public void PlaceBlockGameObject(Block b, int x, int y)
    {
        GameObject prefab = prefabManager.GetBlockPrefab(b.TypeName);

        GameObject go = GameObject.Instantiate(prefab, JKBRCordToUnity(x, y, Map.map.getElements()[x][y].height()), Quaternion.identity);
        int id = gameObjectManager.RegisterGameObject(go, prefabManager.GetPrefabIdOfBlock(b.TypeName));

        this.blockGameObjects.Add(id);
    }

    public void RotateRobot(Robot r, bool directionisright)
    {
        Player p = playerByRobot(r);

        gameObjectManager.AddChange(new Change(3, p.RobotObjectID, 0, 1, 0, directionisright ? 90 : -90));
    }

    public  void RemovePlayer(Robot r)
    {
        Player dead = this.playerByRobot(r);

        JKBRServer.Instance.InformAboutDeath(dead);

        this.players.Remove(dead);

        this.gameObjectManager.AddChange(new Change(4, dead.RobotObjectID));
    }

    public void ActivatePlayerInputs()
    {
        foreach (var item in this.players)
        {
            JKBRServer.Instance.ActivatePlayerInput(item, item.RobotObjectID);
        }

        Dictionary<string, bool> nd = new Dictionary<string, bool>();
        foreach (var item in this.playerNameSentCode)
        {
            nd[item.Key] = false;
        }
        this.playerNameSentCode = nd;
    }

    public bool CheckIfGameIsOver()
    {
        if(this.players.Count == 1)
        {
            JKBRServer.Instance.InformAboutWin(this.players[0]);
            SaveMatchResult();
            return true;
        }
        return false;
    }

    public void SendInventories()
    {
        foreach (Player player in this.players)
        {
            JKBRServer.Instance.SendInventory(player);
        }
    }

    public void SaveMatchResult()
    {
        ServerDatabaseConnection.Instance.SaveMatchResult(JKBRServer.Instance.MatchId, this.players[0].Name, roundSimulator.RoundCount, allPlayers);
    }

    #endregion

    #region Utils

    public Player playerByName(string name)
    {
        foreach (var player in this.players)
        {
            if (player.Name.Equals(name))
            {
                return player;
            }
        }
        return null;
    }

    public Player playerByRobot(Robot r)
    {
        foreach (var player in this.players)
        {
            if (player.JKBRRobot == r)
            {
                return player;
            }
        }
        return null;
    }

    #region Static
    
    public static Vector3 JKBRCordToUnity(int x, int y, int height)
    {
        return new Vector3(x * 10f+5, height * 5f, y * (-10f)-5);
    }

    public float DegreesByOrientation(char rot)
    {
        switch (rot)
        {
            case 'O':
                return 90;
            case 'S':
                return 180;
            case 'W':
                return 270;
            default:
                return 0;
        }
    }

    #endregion
   
    
    #endregion


}
