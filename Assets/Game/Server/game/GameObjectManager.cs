﻿using Assets.Server.networking;
using Assets.Server.Warteschlange;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectManager : MonoBehaviour
{
    public static GameObjectManager Instance;

    public JKBRServer Server;

    int idcounter = 0;

    Dictionary<int, GameObject> gameObjects = new Dictionary<int, GameObject>();

    ChangeQueue queue = new ChangeQueue();


    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        while (queue.HasNext)
        {
            Change c = queue.GetFirst();

            switch (c.Type)
            {
                case 2:
                    this.MoveGameObject(c.Goid, new Vector3(c.Param1, c.Param2, c.Param3));
                    break;
                case 3:
                    this.RotateGameObject(c.Goid, c.Param1, c.Param2, c.Param3, c.Param4);
                    break;
                case 4:
                    this.DestroyGameObject(c.Goid);
                    break;
                default:
                    break;
            }
            
        }
    }




    public int RegisterGameObject(GameObject go, int prefabid)
    {
        this.gameObjects.Add(idcounter, go);
        // Change!
        Server.BroadcastChange(new Assets.Server.networking.Change(1, idcounter, go.transform.position.x, go.transform.position.y, go.transform.position.z, prefabid));
        
        return idcounter++;
    }


    public void AddChange(Change c)
    {
        queue.AddChange(c);

        Server.BroadcastChange(c);
    }


    private void MoveGameObject(int goid, Vector3 npos)
    {
        Debug.Log("[GAMEOBJECTMANAGER] BEwege " + goid);
        GameObject go = this.gameObjects[goid];
        go.transform.position = npos;
    }

    public void RotateGameObject(int goid, float ax, float ay, float az, float degree)
    {
        GameObject go = this.gameObjects[goid];
        go.transform.Rotate(new Vector3(ax, ay, az), -degree);
    }


    private void DestroyGameObject(int goid)
    {
        GameObject go = this.gameObjects[goid];
        GameObject.Destroy(go);
    }

    private void AnimateGameObject(int goid)
    {
        throw new NotImplementedException();
    }
}
