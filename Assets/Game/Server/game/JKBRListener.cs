﻿using System.Collections;
using System.Collections.Generic;
using javakarol;
using UnityEngine;

public class JKBRListener : MonoBehaviour, javakarol.UniversialUnitySpecificListener
{

    public static JKBRListener Instance;

    public GameManager gameManager;

    public void blockPlaced(Block b, int x, int y)
    {
        Debug.Log("Place Block");
        gameManager.BlockPlaced(b, x, y);
    }

    public void itemSpawned(Item i, int x, int y)
    {
        // Should do nothing
    }

    public void onDeath(Robot r)
    {
        gameManager.RemovePlayer(r);
    }

    public void onItemPickedUp(Robot r, Item i, int x, int y)
    {
        gameManager.ItemPickedUp(i);

        JKBRServer.Instance.SendInventory(gameManager.playerByRobot(r));
    }

    public void onRobotConsumes(Robot r, Consumable c)
    {
        // TODO
    }

    public void onRobotDoesStep(Robot r, int nx, int ny, int dheight)
    {
        Debug.Log("ROOOOOOOBOT STEP");
        gameManager.RobotMoves(r, nx, ny);
    }

    public void onRobotRotates(Robot r, bool right)
    {
        gameManager.RotateRobot(r, right);
    }

    public void onRobotUsesBuildItem(Robot r, BuildItem b)
    {
        // TODO
    }

    public void onRobotUsesWeapon(Robot r, Weapon w, bool primary)
    {
        // TODO
    }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        Map.map.addBlockPlacedListener(this);
        Map.map.addItemSpawnListener(this);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
