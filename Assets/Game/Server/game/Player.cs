﻿using codecontrol;
using CommandSystem;
using javakarol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Server.game
{
    public class Player
    {

        string name;
        Robot robot;
        int robotObjectID=-1;

        Command command;

        public string Name
        {
            get { return this.name; }
        }
        
        public Robot JKBRRobot
        {
            get { return this.robot; }
            set { this.robot = value; }
        }

        public int RobotObjectID
        {
            get { return this.robotObjectID; }
            set { this.robotObjectID = value; }
        }

        public Player(string name)
        {
            this.name = name;
        }

        public void SetCommand(Command command)
        {
            this.command =command;
        }

        public void DoStep()
        {
            if (this.command != null)
            {
                this.command = command.execute(this.robot);
                Debug.Log("[PLAYER] executed a step");
            }
        }

    }
}
