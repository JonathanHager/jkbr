﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundSimulator : MonoBehaviour
{

    public static float TIME_BETWEEN_FRAMES = 1;
    public static int STEPS_PER_ROUND = 10;

    public GameManager GameManager;

    int stepCounter = STEPS_PER_ROUND;

    float timesincelastupdate;

    // Stats
    int roundCounter = 0;
    public int RoundCount { get => roundCounter; }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timesincelastupdate += Time.deltaTime;
        // Executes a step when time is over
        if (timesincelastupdate >= TIME_BETWEEN_FRAMES)
        {

            if (stepCounter < STEPS_PER_ROUND)
            {
                Debug.Log("[ROUNDSIMULATOR] simuliere schritt " + stepCounter);

                ++stepCounter;
                timesincelastupdate = 0;

                GameManager.ExecuteStep();

                GameManager.SendInventories();

                if (stepCounter == STEPS_PER_ROUND)
                {
                    if (GameManager.CheckIfGameIsOver())
                    {
                        return;
                    }
                    GameManager.ActivatePlayerInputs();
                }
            }
        }
    }

    public void StartRound()
    {
        roundCounter++;
        stepCounter = 0;

        javakarol.Map.map.scaleZoneDown(Mathf.Max(0, roundCounter - 3));
    }
}
