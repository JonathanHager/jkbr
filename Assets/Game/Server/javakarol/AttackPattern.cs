﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
namespace javakarol
{
    public class AttackPattern
    {
        int[][] patternN;
        int[][] patternE;
        int[][] patternS;
        int[][] patternW;

        public AttackPattern(string name)
        {
            patternN = readFile("/" + name + ".csv");
            patternE = rotatePattern(patternN);
            patternS = rotatePattern(patternE);
            patternW = rotatePattern(patternS);
        }
        // Prints the attack pattern to the console
        public static void printPattern(int[][] p)
        {
            for (int i = 0; i < p.Length; i++)
            {
                for (int j = 0; j < p[i].Length; j++)
                {
                    Console.Write(p[j][i] + "  ");
                }
                Console.WriteLine();
            }
        }
        // Rotates the pattern (90�): N -> E -> S -> W
        private int[][] rotatePattern(int[][] input)
        {
            int[][] result = new int[input.Length][];
            for (int x = 0; x < input.Length; x++)
            {
                result[x] = new int[input[x].Length];
            }
                for (int x = 0; x < input.Length; x++)
            {
                for (int y = 0; y < input[x].Length; y++)
                {
                    //	System.out.println(x + "   " + y);
                    result[input[x].Length - y - 1][x] = input[x][y];
                }
            }
            return result;
        }

        // returns the damage for a field in the pattern
        public int getDmg(int x, int y, char facing)
        {
            switch (facing)
            {
                case 'N':
                    return patternN[x][y];
                case 'E':
                    return patternE[x][y];
                case 'S':
                    return patternS[x][y];
                case 'W':
                    return patternW[x][y];
                default:
                    return 0;
            }
        }

        // returns the pattern for a specific facing
        public int[][] getPattern(char facing)
        {
            switch (facing)
            {
                case 'N':
                    return patternN;
                case 'E':
                    return patternE;
                case 'S':
                    return patternS;
                case 'W':
                    return patternW;
                default:
                    return null;
            }
        }

        private static int[][] readFile(String path)
        {
            using (StreamReader reader = new StreamReader("./resources" + path))
            {
                LinkedList<String> lines = new LinkedList<String>();
                String s = "";
                while ((s = reader.ReadLine()) != null)
                {
                    lines.AddLast(s);
                }
                int[][] pattern = new int[lines.Count][];
                for (int x = 0; x < lines.Count; x++)
                {
                    pattern[x] = new int[lines.Count];
                }
                for (int x = 0; x < lines.Count; x++)
                {
                    String[] worthes = lines.ElementAt(x).Split(',');

                    //Debug.Log(Application.dataPath + "/resources" + path);

                    for (int y = 0; y < lines.Count; y++)
                    {
                        pattern[y][x] = int.Parse(worthes[y]);
                    }
                }
                return pattern;
            }
        }
    }
}