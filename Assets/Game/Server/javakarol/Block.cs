﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public class Block
    {
        // Block Types
        public static Block BLOCK = new Block("BLOCK", true, null);
        public static Block BATMAN_HAT = new Block("BATMAN_HAT", false, Effect.PAIN);
        public static Block ZONE = new Block("ZONE", false, Effect.ZONE);
        public static Block TRAP = new Block("TRAP", false, Effect.STUN);
        public static Block FIRE = new Block("FIRE", false, Effect.FIRE);


        string blocktypename;
        public string TypeName { get { return blocktypename; } }

        private Block(string type, bool solid, Effect effect)
        {
            this.blocktypename = type;

            this.solid = solid;
            this.effect = effect;
        }

        private bool solid;
        private Effect effect;

        public bool isSolid()
        {
            return solid;
        }

        public Effect getEffect()
        {
            return effect;
        }
    }
}
