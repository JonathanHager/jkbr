﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public class BuildItem : Item
    {
        // Each type of build items has a block to place
        private Block block;

        // How often the item can build (item disappears if amount == 0)
        private int amount;

        // Server
        public BuildItem(String name, int x, int y) : base(name,x,y)
        {

            switch (name)
            {
                case "item_trap_batman_hat":
                    block = Block.BATMAN_HAT;
                    this.amount = ServerConfiguration.BATMAN_TRAP_AMOUNT;
                    break;
               // case "item_block":
                 //   block = Block.BLOCK;
                   // this.amount = ServerConfiguration.BLOCK_BUILDER_AMOUNT;
                    //break;
            }
        }

        // client
        public BuildItem(String name, int x, int y, int amount):this(name,x,y)
        {
            this.amount = amount;
        }

        public Block getBlock()
        {
            return block;
        }

        // Returns wether the item is removed form inventory when it is used 
        public bool isReusable()
        {
            return amount > 0;
        }

        // Reduces the amount (called on use of item)
        public void decreaseAmount(int i)
        {
            this.amount -= i;
        }
        public void decreaseAmount()
        {
            decreaseAmount(1);
        }

        public void setAmount(int amount)
        {
            this.amount = amount;
        }
        public int getAmount()
        {
            return amount;
        }
    }
}
