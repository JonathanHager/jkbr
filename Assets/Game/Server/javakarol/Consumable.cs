﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public class Consumable : Item
    {
        private Effect effect;
        private int amount;

        // Server
        public Consumable(String name, int x, int y) : base(name,x,y)
        {

            switch (name)
            {
                case "apple":
                    this.effect = Effect.HEAL;
                    this.amount = ServerConfiguration.APPLE_AMOUNT;
                    break;
            }
        }

        // client
        public Consumable(String name, int x, int y, int amount): this(name,x,y)
        {
            this.amount = amount;
        }


        public Effect getEffect()
        {
            return effect;
        }

        public void setEffect(Effect effect)
        {
            this.effect = effect;
        }

        public int getAmount()
        {
            return amount;
        }

        public void setAmount(int amount)
        {
            this.amount = amount;
        }

    }
}
