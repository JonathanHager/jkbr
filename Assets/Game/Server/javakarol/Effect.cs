﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public class Effect
    {
        // Effects 
        public static Effect PAIN = new Effect(-100, false, 1); // Makes direct damage
        public static Effect FIRE = new Effect(-5, false, 5); // Makes damage over time
        public static Effect STUN = new Effect(-1, true, 5); // Makes the robot not moving
        public static Effect HEAL = new Effect(4, false, 5); // Heals the robot
        public static Effect ZONE = new Effect(-1, false, 2); // Zone damages every step 

        // effectDuration in steps
        private Effect(int health, bool stun, int effectDuration)
        {
            this.health = health;
            this.stun = stun;
            this.effectDuration = effectDuration;
        }

        private int health;
        private bool stun;
        private int effectDuration;

        public int getHealth()
        {
            return health;
        }

        public bool getStun()
        {
            return stun;
        }


        public int getEffectDuration()
        {
            return effectDuration;
        }
    }
}
