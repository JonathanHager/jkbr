﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace javakarol
{
    public class Entity
    {

        public Entity(int x, int y)
        {


            if (x >= 0 && y >= 0)
                Map.map.addEntity(this, x, y);
        }

        public virtual void recieveDamage(int damage)
        {
        }
    }
}
