﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace javakarol
{
    public class Item : Entity
    {
        protected String name;

        public Item(String name, int x, int y) :base(x,y)
        {
            this.name = name;
            Map.map.itemAdded(this,x,y);
        }

        public String getName()
        {
            return name;
        }
    }
}
