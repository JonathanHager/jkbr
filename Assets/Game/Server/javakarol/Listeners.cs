﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public interface DeathListener
    {
        void onDeath(Robot r);
    }

    public interface RobotUsesWeaponListener
    {
        void onRobotUsesWeapon(Robot r, Weapon w, bool primary);
    }

    public interface RobotConsumesConsumableListener
    {
        void onRobotConsumes(Robot r, Consumable c);
    }

    public interface RobotUsesBuildItemListener
    {
        void onRobotUsesBuildItem(Robot r, BuildItem b);
    }

    public interface RobotDoesStepListener
    {
        void onRobotDoesStep(Robot r, int nx, int ny, int dheight);
    }

    public interface RobotRotatesListener
    {
        void onRobotRotates(Robot r, bool right);
    }

    public interface ItemPickedUpListener
    {
        void onItemPickedUp(Robot r,Item i, int x, int y);
    }

    public interface SpawnListener
    {
        void entitySpawned(Entity e, int x, int y);
    }

    public interface ItemSpawnedListener
    {
        void itemSpawned(Item i, int x, int y);
    }

    public interface BlockPlacedListener
    {
        void blockPlaced(Block b, int x, int y);
    }


    public interface UniversialUnitySpecificListener : DeathListener, RobotUsesWeaponListener, RobotUsesBuildItemListener, RobotConsumesConsumableListener, RobotDoesStepListener, RobotRotatesListener, ItemPickedUpListener, ItemSpawnedListener, BlockPlacedListener
    {

    }
}
