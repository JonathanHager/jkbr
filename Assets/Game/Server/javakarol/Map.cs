﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace javakarol
{
    class Map
    {
        private MapElement[][] elements;

        public static Map map = new Map(30, 30);

        // Listeners for every shit
        List<SpawnListener> spawnListeners;

        internal void itemAdded(Item e, int x, int y)
        {
           // Debug.Log("[MAP] " + " it's a item.");
            foreach (var item in this.itemSpawnedListeners)
            {
                item.itemSpawned(e, x, y);
            }
        }

        List<ItemSpawnedListener> itemSpawnedListeners;
        List<BlockPlacedListener> blockPlacedListeners;
        

        private Map(int width, int height)
        {
            elements = new MapElement[width][];
            for (int x = 0; x < elements.Length; x++)
            {
                elements[x] = new MapElement[height];
                for (int y = 0; y < elements[x].Length; y++)
                {
                    elements[x][y] = new MapElement();
                }
            }

            this.spawnListeners = new List<SpawnListener>();
            this.itemSpawnedListeners = new List<ItemSpawnedListener>();
            this.blockPlacedListeners = new List<BlockPlacedListener>();
        }

        // Reinits the map
        public static void initNewMap(int x, int y)
        {
            Map.map = new Map(x, y);
        }


        // Calculates which Entities are attacked by the Attack pattern
        // Makes the selected entities receive the damage
        public void dealDamage(Robot r, Weapon w, bool primary)
        {
            if (w == null)
            {
                return;
            }
            AttackPattern tmp;
            if (primary)
            {
                tmp = w.pattern1;
            }
            else
            {
                tmp = w.pattern2;
            }
            int startx = r.getX() - tmp.getPattern('N').Length / 2;
            int starty = r.getY() - tmp.getPattern('N').Length / 2;
            MapElement[][] attackedElements = new MapElement[tmp.getPattern('N').Length][];
            for (int x = startx; x < attackedElements.Length + startx; x++)
            {
                // CHANGE 2019-06-04 => I think this was the problem on another point (added "-startx")
                attackedElements[x - startx] = new MapElement[tmp.getPattern('N').Length];
                for (int y = starty; y < attackedElements[0].Length + starty; y++)
                {
                    if (x < elements[0].Length && y < elements.Length && x >= 0 && y >= 0
                            // height -> robot cant hit over his head (3 blocks high)
                            && elements[x][y].height() < elements[r.getX()][r.getY()].height() + ServerConfiguration.ROBOT_HIT_HEIGHT)
                        attackedElements[x - startx][y - starty] = elements[x][y];
                }
            }
            for (int x = 0; x < attackedElements.Length; x++)
            {
                for (int y = 0; y < attackedElements[0].Length; y++)
                {
                    if (attackedElements[x][y] == null || attackedElements[x][y].getEntities() == null)
                    {
                        continue;
                    }
                    for (int i = 0; i < attackedElements[x][y].getEntities().Count; i++)
                    {
                        //					System.out.println(
                        //							"Angriff auf " + attackedElements[x][y].getEntities().get(i) + "       " + x + "    " + y);
                        if (attackedElements[x][y].getEntities().ElementAt(i).GetType() == typeof(Robot))
                        {
                            int dmg = tmp.getDmg(x, y, r.getFacing());
                            attackedElements[x][y].getEntities().ElementAt(i).recieveDamage(dmg);
                        }
                    }
                }
            }

            // #UNITY
        //    mapController.DealDamage(r, w, primary);
        }

        // Returns the map Elements which the robot can reach (in "circle" with radius r.getRange())
        // Is used for sensors
        public MapElement[][] getMapElementsNearBy(Robot r)
        {
            MapElement[][] nearElements = new MapElement[r.getRange() * 2 + 1][];
            for (int x = r.getX() - r.getRange(); x < nearElements.Length + r.getX() - r.getRange(); x++)
            {
                    nearElements[x-r.getX()+r.getRange()] = new MapElement[r.getRange() * 2 + 1];
                    for (int y = r.getY() - r.getRange(); y < nearElements[x-r.getX() + r.getRange()].Length + r.getY() - r.getRange(); y++)
                    {
                        if (x >= 0 && x < elements[0].Length && y >= 0 && y < elements.Length)
                            nearElements[x - r.getX() + r.getRange()][y - r.getY() + r.getRange()] = elements[x][y];
                    }
                
            }
            return nearElements;
        }

        // Moves the robot to the next map element and returns if it's possible
        public bool doStep(Robot r)
        {
            switch (r.getFacing())
            {
                case 'N':
                    //			System.out.println(
                    //					r.getX() + "   " + r.getY() + "      " + elements[r.getX()][r.getY()].getEntities().Count);
                    if (r.getY() - 1 >= 0
                            && !elements[r.getX()][r.getY() - 1].getEntities().Any(e => e.GetType() == typeof(Robot))
                            && Math.Abs(elements[r.getX()][r.getY() - 1].height()
                                    - elements[r.getX()][r.getY()].height()) <= r.getJumpHeight())
                    {
                        elements[r.getX()][r.getY()].getEntities().Remove(r);
                        elements[r.getX()][r.getY() - 1].getEntities().AddLast(r);
                        
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case 'E':
                    //			System.out.println(
                    //					r.getX() + "   " + r.getY() + "      " + elements[r.getX()][r.getY()].getEntities().Count);
                    if (r.getX() + 1 < elements.Length
                            && !elements[r.getX() + 1][r.getY()].getEntities().Any(e => e.GetType() == typeof(Robot))
                            && Math.Abs(elements[r.getX() + 1][r.getY()].height()
                                    - elements[r.getX()][r.getY()].height()) <= r.getJumpHeight())
                    {
                        elements[r.getX()][r.getY()].getEntities().Remove(r);
                        elements[r.getX() + 1][r.getY()].getEntities().AddLast(r);
                        
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case 'S':
                    //			System.out.println(
                    //					r.getX() + "   " + r.getY() + "      " + elements[r.getX()][r.getY()].getEntities().Count);
                    if (r.getY() + 1 < elements[0].Length
                            && !elements[r.getX()][r.getY() + 1].getEntities().Any(e => e.GetType() == typeof(Robot))
                            && Math.Abs(elements[r.getX()][r.getY() + 1].height()
                                    - elements[r.getX()][r.getY()].height()) <= r.getJumpHeight())
                    {
                        elements[r.getX()][r.getY()].getEntities().Remove(r);
                        elements[r.getX()][r.getY() + 1].getEntities().AddLast(r);
                        
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case 'W':
                    //			System.out.println(
                    //					r.getX() + "   " + r.getY() + "      " + elements[r.getX()][r.getY()].getEntities().Count);
                    if (r.getX() - 1 >= 0
                            && !elements[r.getX() - 1][r.getY()].getEntities().Any(e => e.GetType() == typeof(Robot))
                            && Math.Abs(elements[r.getX() - 1][r.getY()].height()
                                    - elements[r.getX()][r.getY()].height()) <= r.getJumpHeight())
                    {
                        elements[r.getX()][r.getY()].getEntities().Remove(r);
                        elements[r.getX() - 1][r.getY()].getEntities().AddLast(r);
                        
                        //Debug.Log("Step west");

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                default:
                    return false;
            }
        }

        // Uses the effects of the top block on the robot
        public void robotOnMapElement(Robot r, MapElement me)
        {
            Block top = me.getTop();
            if (top != null)
                handleEffects(r, top.getEffect());
        }

        // Handles the effects on a robot
        public void handleEffects(Robot r, Effect e)
        {
            if (e == null)
            {
                return;
            }

            r.effect(e);
            
        }

        // Places a block on a elements
        // Is only possible if block on top is solid
        // Returns if placing a block is possible
        public bool placeBlock(int x, int y, Block block)
        {
            MapElement me = elements[x][y];
            if (me.getTop() != null && !me.getTop().isSolid())
            {
                return false;
            }
            me.getBlocks().Push(block);

            // Call listener
            foreach (var item in this.blockPlacedListeners)
            {
                item.blockPlaced(block, x, y);
            }

            return true;
        }

        // returns the map
        public Map getMap()
        {
            return map;
        }

        // returns the map elements
        public MapElement[][] getElements()
        {
            return elements;
        }

        // Spawns a entity e on the position x y
        public void addEntity(Entity e, int x, int y)
        {
            if (e != null)
                elements[x][y].addEntity(e);

            // call listener
            foreach (var item in this.spawnListeners)
            {
                item.entitySpawned(e,x, y);
            }
            //Debug.Log("[MAP] " + "Adde Entity");
        }

        // More simple interface for getting next robot element
        public MapElement getElementBeforeRobot(Robot r)
        {
            int x = robotsNextX(r);
            int y = robotsNextY(r);

            // Robot cant do a step
            if (x == r.getX()
                    && y == r.getY())
            {
                return null;
            }

            return elements[x][y];
        }

        // Next x value, if robot makes a step
        public int robotsNextX(Robot r)
        {
            int x = r.getX();
            char facing = r.getFacing();

            switch (facing)
            {
                case 'E':
                    ++x;
                    break;
                case 'W':
                    --x;
                    break;
            }

            if (x < 0 || x >= elements.Length)
            {
                return r.getX();
            }
            return x;
        }
        // Next y value if robot makes a step
        public int robotsNextY(Robot r)
        {

            int y = r.getY();
            char facing = r.getFacing();

            switch (facing)
            {
                case 'N':
                    --y;
                    break;
                case 'S':
                    ++y;
                    break;
            }

            if (y < 0 || y >= elements[0].Length)
            {
                return r.getY();
            }
            return y;
        }

        // scales the zone down (elements which are reduced in each direction
        public void scaleZoneDown(int elements)
        {
            for (int i = 1; i < elements; i++)
            {

                for (int x = i - 1; x < this.elements.Length - (i - 1); x++)
                {
                    spawnZone(x, i - 1);
                    spawnZone(x, this.elements.Length - i);
                }
                for (int y = 0; y < this.elements.Length - i; y++)
                {
                    spawnZone(i - 1, y);
                    spawnZone(this.elements.Length - i, y);
                }

            }
        }
        private void spawnZone(int x, int y)
        {
            MapElement me = this.elements[x][y];
            if (me.getTop() != null && !me.getTop().isSolid())
            {
                me.getBlocks().Pop();
            }
            LinkedList<Robot> robots = new LinkedList<Robot>();
            foreach (Entity e in me.getEntities())
            {
                if (e.GetType() == typeof(Robot))
                {
                    robots.AddLast((Robot)e);
                }
            }
            me.getEntities().Clear();
            foreach (Robot r in robots)
            {
                me.addEntity(r);
            }
            placeBlock(x, y, Block.ZONE);
            
        }


        // Add listeners
        public void addSpawnListener(SpawnListener l)
        {
            this.spawnListeners.Add(l);
        }
        public void addItemSpawnListener(ItemSpawnedListener l)
        {
            this.itemSpawnedListeners.Add(l);
        }
        public void addBlockPlacedListener(BlockPlacedListener l)
        {
            this.blockPlacedListeners.Add(l);
        }
    }
}
