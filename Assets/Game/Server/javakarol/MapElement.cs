﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    class MapElement
    {
        private Stack<Block> blocks;

        // List of Entities on MapElement
        private LinkedList<Entity> entities;

        public MapElement()
        {
            blocks = new Stack<Block>();
            entities = new LinkedList<Entity>();
        }

        // Returns the block stack
        public Stack<Block> getBlocks()
        {
            return this.blocks;
        }

        // Return all entities on MapElement
        public LinkedList<Entity> getEntities()
        {
            return this.entities;
        }

        // Returns the first element in the stack
        public Block getTop()
        {
            if (blocks.Count == 0)
            {
                return null;
            }
            return blocks.Last();
        }

        // Adds a entity on a MapElement
        public void addEntity(Entity e)
        {
            entities.AddLast(e);
        }

        // height of blocks (only solid blocks count, because on this height the entities stand
        public int height()
        {
            int i = 0;
            foreach (Block b in blocks)
            {
                if (b.isSolid())
                {
                    ++i;
                }
            }
            return i;
        }
    }
}
