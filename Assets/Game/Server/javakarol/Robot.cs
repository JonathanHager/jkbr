﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace javakarol
{
    public class Robot : Entity
    {
        private char facing; // 'N','E','S','W'
                             // Slots for Weapons
        private Weapon[] weaponSlots;
        // Slots for Consumables
        private Consumable[] consumableSlots;
        // Slots for BuildItems
        private BuildItem[] buildSlots;
        // Position of robot
        private int x, y;
        // Distance in which the robot can see (sensors)
        private int range;
        // Name of the player whose robot this is
        private String name;
        // Jump height of robots
        private int jumpHeight;

        // Observer to inform player about the death of his robot
        private List<DeathListener> deathListener;

        // Listener who informs the server when a weapon is used
        private List<RobotUsesWeaponListener> weaponUseListener;

        // Listener which waits for the robot to build a block
        private List<RobotUsesBuildItemListener> buildItemUseListener;

        // Listener which waits for the robot consume
        private List<RobotConsumesConsumableListener> consumeListener;

        // Listener which waits for steps of the Robot
        private List<RobotDoesStepListener> stepListeners;

        // listener which waits wether the robot rotates
        private List<RobotRotatesListener> rotationListeners;

        List<ItemPickedUpListener> itemPickedUpListeners;

        // Robots health (if 0 -> death)
        private int health;

        // Effects (duration which the effects will be on the robot)
        private int pain;
        private int stun;
        private int fire;
        private int heal;
        private int zone;

        public Robot() : this(0, 0, "javaman", 'N')
        {

        }

        public Robot(int x, int y, String name, char facing) : base(x, y)
        {
            //    Debug.Log("Robot macht was");
            this.x = x;
            this.y = y;
            this.name = name;
            this.facing = facing;
            this.range = 1;
            this.jumpHeight = ServerConfiguration.ROBOT_JUMP_HEIGHT;
            this.weaponSlots = new Weapon[ServerConfiguration.WEAPON_SLOT_COUNT];
            this.consumableSlots = new Consumable[ServerConfiguration.CONSUMABLE_SLOT_COUNT];
            this.buildSlots = new BuildItem[ServerConfiguration.BUILDITEM_SLOT_COUNT];
            this.health = ServerConfiguration.ROBOT_START_HEALTH;

            // Listeners who are waiting for the roboters death
            this.deathListener = new List<DeathListener>();

            // Listener for weapon uses
            this.weaponUseListener = new List<RobotUsesWeaponListener>();

            // Builditem listener
            this.buildItemUseListener = new List<RobotUsesBuildItemListener>();

            // consume listeenr
            this.consumeListener = new List<RobotConsumesConsumableListener>();

            // step listener
            this.stepListeners = new List<RobotDoesStepListener>();

            // rotation listener
            this.rotationListeners = new List<RobotRotatesListener>();

            // item pickup listener
            this.itemPickedUpListeners = new List<ItemPickedUpListener>();

            //     Debug.Log("Robot ist ready");
        }

        public void init(int x, int y, string name, char facing)
        {
            this.x = x;
            this.y = y;
            this.name = name;
            this.facing = facing;
        }

        // MOVEMENT (function is obvious)
        public bool left()
        {
            switch (facing)
            {
                case 'N':
                    facing = 'W';
                    break;
                case 'W':
                    facing = 'S';
                    break;
                case 'S':
                    facing = 'E';
                    break;
                case 'E':
                    facing = 'N';
                    break;

            }
            // Rotation -> Listener
            foreach (var item in rotationListeners)
            {
                item.onRobotRotates(this, false);
            }

            return true;
        }

        public bool right()
        {
            switch (facing)
            {
                case 'N':
                    facing = 'E';
                    break;
                case 'W':
                    facing = 'N';
                    break;
                case 'S':
                    facing = 'W';
                    break;
                case 'E':
                    facing = 'S';
                    break;

            }
            // Rotation -> Listener
            foreach (var item in rotationListeners)
            {
                item.onRobotRotates(this, true);
            }
            Debug.Log(facing);
            return true;
        }

        public bool forward()
        {

            // stun effect active
            if (stun > 0)
            {
                return false;
            }
            if (Map.map.doStep(this))
            {
                Debug.Log("ROBOT MAP STEP");
                int ox = x, oy = y;
                // for listener
                switch (facing)
                {
                    case 'N':
                        y--;
                        break;
                    case 'W':
                        x--;
                        break;
                    case 'S':
                        y++;
                        break;
                    case 'E':
                        x++;
                        break;

                }

                // step -> Listener
                foreach (var item in stepListeners)
                {
                    item.onRobotDoesStep(this, x, y, Map.map.getElements()[x][y].height() - Map.map.getElements()[x][y].height());
                }
                return true;
            }
            else
            {
                Debug.Log("Kann keinen Schritt machen");
                return false;
            }
        }

        // ITEM_USE
        public void attack(int weaponSlot, bool primary)
        {
            // Inform the RobotUsesWeaponListener
            if (this.weaponSlots[weaponSlot] != null)
            {
                foreach (RobotUsesWeaponListener l in this.weaponUseListener)
                {
                    l.onRobotUsesWeapon(this, this.weaponSlots[weaponSlot], primary);
                }
            }
            Map.map.dealDamage(this, this.weaponSlots[weaponSlot], primary);
        }

        public void consume(int i)
        {
            if (consumableSlots[i] == null)
            {
                return;
            }
            Effect eft = consumableSlots[i].getEffect();

            Map.map.handleEffects(this, eft);


            consumableSlots[i].setAmount(consumableSlots[i].getAmount());

            // call listener
            foreach (RobotConsumesConsumableListener l in this.consumeListener)
            {
                l.onRobotConsumes(this, consumableSlots[i]);
            }
            if (consumableSlots[i].getAmount() <= 0)
            {
                clearConsumableSlot(i);
            }

        }

        public void placeBlock(int i)
        {
            if (buildSlots[i] == null)
            {
                return;
            }
            MapElement nextElement = Map.map.getElementBeforeRobot(this);
            if (nextElement != null)
            {
                int x = Map.map.robotsNextX(this);
                int y = Map.map.robotsNextY(this);
                bool success = Map.map.placeBlock(x, y, buildSlots[i].getBlock());
                if (success)
                {
                    buildSlots[i].decreaseAmount();
                    // Build Item use litener
                    foreach (RobotUsesBuildItemListener l in this.buildItemUseListener)
                    {
                        l.onRobotUsesBuildItem(this, buildSlots[i]);
                    }
                    // Destroy build item if it's no longer usable
                    if (!buildSlots[i].isReusable())
                    {
                        clearBuildItemSlot(i);
                    }
                }
            }
        }

        public void clearSlot(int slot)
        {
            if (slot < weaponSlots.Length)
            {
                clearWeaponSlot(slot);
            }
            else if (slot < consumableSlots.Length)
            {
                clearConsumableSlot(slot - weaponSlots.Length);
            }
            else if (slot < buildSlots.Length)
            {
                clearBuildItemSlot(slot - weaponSlots.Length - consumableSlots.Length);
            }
        }

        private void clearWeaponSlot(int i)
        {
            weaponSlots[i] = null;
        }

        private void clearConsumableSlot(int i)
        {
            consumableSlots[i] = null;
        }

        private void clearBuildItemSlot(int i)
        {
            buildSlots[i] = null;
        }

        // SENSOR

        public bool isBlock()
        {
            MapElement[][] sight = Map.map.getMapElementsNearBy(this);
            switch (facing)
            {
                case 'N':
                    if (sight[range][range - 1].getTop() == Block.BLOCK)
                    {
                        return true;
                    }
                    break;
                case 'W':
                    if (sight[range - 1][range].getTop() == Block.BLOCK)
                    {
                        return true;
                    }
                    break;
                case 'S':
                    if (sight[range][range + 1].getTop() == Block.BLOCK)
                    {
                        return true;
                    }
                    break;
                case 'E':
                    if (sight[range + 1][range].getTop() == Block.BLOCK)
                    {
                        return true;
                    }
                    break;

            }
            return false;
        }

        public bool isBorder()
        {
            MapElement[][] sight = Map.map.getMapElementsNearBy(this);

            switch (facing)
            {
                case 'N':

                    if (sight[range][range - 1] == null)
                    {
                        return true;
                    }

                    break;
                case 'W':
                    if (sight[range - 1][range] == null)
                    {
                        return true;
                    }
                    //			System.out.println(sight[range - 1][range].toString());
                    break;
                case 'S':
                    if (sight[range][range + 1] == null)
                    {
                        return true;
                    }
                    break;
                case 'E':
                    if (sight[range + 1][range] == null)
                    {
                        return true;
                    }
                    break;

            }
            return false;
        }

        public bool isRobotInAttackRange(int slot, bool primary)
        {
            Weapon w = weaponSlots[slot];
            AttackPattern tmp = null;
            if (primary)
            {
                tmp = w.pattern1;
            }
            else
            {
                tmp = w.pattern2;
            }
            int sx = getX() - tmp.getPattern('N').Length / 2;
            int sy = getY() - tmp.getPattern('N').Length / 2;
            MapElement[][] attackedElements = new MapElement[tmp.getPattern('N').Length][];
            for (int x = sx; x < attackedElements.Length + sx; x++)
            {
                attackedElements[x] = new MapElement[tmp.getPattern('N').Length];
                for (int y = sy; y < attackedElements[0].Length + sy; y++)
                {
                    if (x < Map.map.getElements()[0].Length && y < Map.map.getElements().Length && x >= 0 && y >= 0)
                        attackedElements[x - sx][y - sy] = Map.map.getElements()[x][y];
                }
            }
            for (int i = 0; i < attackedElements.Length; i++)
            {
                for (int j = 0; j < attackedElements[i].Length; j++)
                {
                    if (attackedElements[i][j] == null)
                    {
                        continue;
                    }
                    LinkedList<Entity> list = attackedElements[i][j].getEntities();
                    for (int k = 0; k < list.Count; k++)
                    {
                        if (list.ElementAt(k).GetType() == typeof(Robot) && list.ElementAt(k) != this && tmp.getDmg(i, j, facing) > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public LinkedList<String> getNamesOfRobotInSight()
        {
            MapElement[][] sight = Map.map.getMapElementsNearBy(this);
            LinkedList<String> names = new LinkedList<String>();
            LinkedList<Entity> ent;
            for (int i = 0; i < sight.Length; i++)
            {
                for (int j = 0; j < sight.Length; j++)
                {
                    ent = sight[i][j].getEntities();
                    for (int k = 0; k < ent.Count; k++)
                    {
                        if (ent.ElementAt(k).GetType() == typeof(Robot))
                        {
                            names.AddLast(((Robot)ent.ElementAt(k)).getName());
                        }
                    }
                }
            }
            return names;
        }

        public bool isRobotInFrontOfMe()
        {
            MapElement[][] sight = Map.map.getMapElementsNearBy(this);
            LinkedList<Entity> ent;
            switch (facing)
            {
                case 'N':
                    ent = sight[range][range - 1].getEntities();
                    if (isRobotInList(ent))
                        return true;
                    break;

                case 'W':
                    ent = sight[range - 1][range].getEntities();
                    if (isRobotInList(ent))
                        return true;
                    break;

                case 'S':
                    ent = sight[range][range + 1].getEntities();
                    if (isRobotInList(ent))
                        return true;
                    break;

                case 'E':
                    ent = sight[range + 1][range].getEntities();
                    if (isRobotInList(ent))
                        return true;
                    break;

            }
            return false;
        }

        // Can say if robot is in list of entities
        private bool isRobotInList(LinkedList<Entity> ent)
        {
            for (int k = 0; k < ent.Count; k++)
            {
                if (ent.ElementAt(k).GetType() == typeof(Robot))
                {
                    return true;
                }
            }
            return false;
        }

        // Returns Robot nearby this robot by name
        public Robot getRobot(String name)
        {
            MapElement[][] sight = Map.map.getMapElementsNearBy(this);
            LinkedList<Entity> ent;
            for (int i = 0; i < sight.Length; i++)
            {
                for (int j = 0; j < sight.Length; j++)
                {
                    ent = sight[i][j].getEntities();
                    for (int k = 0; k < ent.Count; k++)
                    {
                        if (ent.ElementAt(k).GetType() == typeof(Robot))
                        {
                            if (((Robot)ent.ElementAt(k)).getName().Equals(name))
                            {
                                return (Robot)ent.ElementAt(k);
                            }
                        }
                    }
                }
            }
            return null;
        }

        // INTERACTION
        // Picks up an item and pushes it in the right slot
        public bool pickUp(int number)
        {

            LinkedList<Entity> ent = Map.map.getElements()[x][y].getEntities();
            LinkedList<Entity> cpy = new LinkedList<Entity>(ent);
            foreach (Entity e in ent)
            {
                if (e.GetType() == typeof(Robot))
                {
                    cpy.Remove(e);
                }
            }

            List<Entity> res = new List<Entity>();
            // Remove not wanted stuff
            if (number < weaponSlots.Length)
            {
                res = cpy.Where(e => e.GetType() == typeof(Weapon)).ToList();
            }
            else if (number < weaponSlots.Length + consumableSlots.Length)
            {
                res = cpy.Where(e => e.GetType() == typeof(Consumable)).ToList();
            }
            else if (number < weaponSlots.Length + consumableSlots.Length + buildSlots.Length)
            {
                res = cpy.Where(e => e.GetType() == typeof(BuildItem)).ToList();
            }

            if (res.Count > 0)
            {
                Item i = (Item)res.ElementAt(0);
                if (number < weaponSlots.Length)
                {
                    if (weaponSlots[number] != null)
                    {
                        Map.map.addEntity(weaponSlots[number], x, y);
                    }
                    weaponSlots[number] = (Weapon)i;
                }
                else if (number < weaponSlots.Length + consumableSlots.Length)
                {
                    if (consumableSlots[number - weaponSlots.Length] != null)
                    {
                        if (i.getName().Equals(consumableSlots[number - weaponSlots.Length].getName()))
                        {
                            consumableSlots[number - weaponSlots.Length]
                                    .setAmount(consumableSlots[number - weaponSlots.Length].getAmount()
                                            + ((Consumable)i).getAmount());
                        }
                        else
                        {
                            Map.map.addEntity(consumableSlots[number - weaponSlots.Length], x, y);
                            consumableSlots[number - weaponSlots.Length] = (Consumable)i;
                        }
                    }
                    else
                    {
                        consumableSlots[number - weaponSlots.Length] = (Consumable)i;
                    }
                }
                else if (number < weaponSlots.Length + consumableSlots.Length + buildSlots.Length)
                {
                    if (buildSlots[number - weaponSlots.Length - consumableSlots.Length] != null)
                    {
                        if (i.getName()
                                .Equals(buildSlots[number - weaponSlots.Length - consumableSlots.Length].getName()))
                        {
                            buildSlots[number - weaponSlots.Length - consumableSlots.Length]
                                    .setAmount(buildSlots[number - weaponSlots.Length - consumableSlots.Length].getAmount()
                                            + ((BuildItem)i).getAmount());
                        }
                        else
                        {
                            Map.map.addEntity(buildSlots[number - weaponSlots.Length - consumableSlots.Length], x, y);
                            buildSlots[number - weaponSlots.Length - consumableSlots.Length] = (BuildItem)i;
                        }
                    }
                    else
                    {
                        buildSlots[number - weaponSlots.Length - consumableSlots.Length] = (BuildItem)i;
                    }
                }
                ent.Remove(res.ElementAt(0));

                // call listener
                foreach (var item in this.itemPickedUpListeners)
                {
                    item.onItemPickedUp(this, (Item)res.ElementAt(0), this.x, this.y);
                }
            }

            return false;
        }

        // Robot receives damage
        public override void recieveDamage(int damage)
        {
            health = health - damage;
            if (health <= 0)
            {
                this.kill();
            }
        }

        // Kills this robot
        public void kill()
        {
            MapElement tmp = Map.map.getElements()[this.x][this.y];
            for (int i = 0; i < tmp.getEntities().Count; i++)
            {
                if (tmp.getEntities().ElementAt(i).Equals(this))
                {
                    Map.map.getElements()[this.x][this.y].getEntities().Remove(Map.map.getElements()[this.x][this.y].getEntities().ElementAt(i));
                }
            }
            foreach (Weapon w in weaponSlots)
            {
                Map.map.addEntity(w, this.x, this.y);
            }
            foreach (Consumable b in consumableSlots)
            {
                Map.map.addEntity(b, this.x, this.y);
            }
            foreach (BuildItem b in buildSlots)
            {
                Map.map.addEntity(b, this.x, this.y);
            }

            if (deathListener != null)
            {
                foreach (DeathListener d in deathListener)
                {
                    d.onDeath(this);
                }
            }
        }

        // Getter
        public int getRange()
        {
            return range;
        }

        public String getName()
        {
            return name;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public char getFacing()
        {
            return facing;
        }

        public int getJumpHeight()
        {
            return jumpHeight;
        }

        public int getHealth()
        {
            return health;
        }

        public Weapon[] getWeaponSlots()
        {
            return weaponSlots;
        }

        public Consumable[] getConsumableSlots()
        {
            return consumableSlots;
        }

        public BuildItem[] getBuildSlots()
        {
            return buildSlots;
        }

        public void addDeathListener(DeathListener d)
        {
            deathListener.Add(d);
        }

        public void addWeaponUseListener(RobotUsesWeaponListener l)
        {
            this.weaponUseListener.Add(l);
        }

        public void addBuildItemUseListener(RobotUsesBuildItemListener l)
        {
            this.buildItemUseListener.Add(l);
        }

        public void addConsumeListener(RobotConsumesConsumableListener l)
        {
            this.consumeListener.Add(l);
        }
        public void addStepListener(RobotDoesStepListener l)
        {
            this.stepListeners.Add(l);
        }
        public void addRotationListener(RobotRotatesListener l)
        {
            this.rotationListeners.Add(l);
        }
        public void addItemPickedUpListener(ItemPickedUpListener l)
        {
            this.itemPickedUpListeners.Add(l);
        }

        // Should be called in each step
        public void update()
        {
            Map.map.robotOnMapElement(this, Map.map.getElements()[x][y]);
            updateEffects();
        }

        private void updateEffects()
        {
            if (this.pain > 0)
            {
                this.recieveDamage(-Effect.PAIN.getHealth());
                --pain;
            }
            if (this.stun > 0)
            {
                --stun;
            }
            if (this.fire > 0)
            {
                this.recieveDamage(-Effect.FIRE.getHealth());
                --fire;
            }
            if (this.heal > 0)
            {
                this.recieveDamage(-Effect.HEAL.getHealth());
                --this.heal;
            }
            if (this.zone > 0)
            {
                this.recieveDamage(-Effect.ZONE.getHealth());
                --this.zone;
            }
        }

        public void effect(Effect e)
        {
            if (e == Effect.PAIN)
            {
                this.pain = e.getEffectDuration();
            }
            else if (e == Effect.STUN)
            {
                this.stun = e.getEffectDuration();
            }
            else if (e == Effect.FIRE)
            {
                this.fire = e.getEffectDuration();
            }
            else if (e == Effect.HEAL)
            {
                this.heal = e.getEffectDuration();
            }
            else if (e == Effect.ZONE)
            {
                this.zone = e.getEffectDuration();
            }
        }

    }
}
