﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    class ServerConfiguration
    {
        internal static int WEAPON_SLOT_COUNT = 2;
        internal static int CONSUMABLE_SLOT_COUNT = 1;
        internal static int BUILDITEM_SLOT_COUNT= 1;
        internal static int ROBOT_START_HEALTH= 100;
        internal static int ROBOT_HIT_HEIGHT= 3;
        internal static int ROBOT_JUMP_HEIGHT = 1;
        internal static int APPLE_AMOUNT=5;
        internal static int BATMAN_TRAP_AMOUNT=1;
        internal static int BLOCK_BUILDER_AMOUNT=5;
    }
}
