﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace javakarol
{
    public class Weapon : Item
    {

        public Weapon(String name, int x, int y) : base(name,x,y)
        {
            this.pattern1 = new AttackPattern(name + "0");
            this.pattern2 = new AttackPattern(name + "1");
        }

        public AttackPattern pattern1;
        public AttackPattern pattern2;
    }
}
