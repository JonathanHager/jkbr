﻿using Assets.Server.game;
using Assets.Server.networking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using Assets.Network;
using Assets;

public class JKBRServer : MonoBehaviour
{

    public int dataport = 32002;
    public int worldport = 32001;


    public static int DATA_SIZE = 1024 * 5;

    public static int MAX_CONNECTIONS = 5;

    public static JKBRServer Instance;

    public GameManager gameManager;

    // To Stream the world
    private static Socket _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

    private static List<Socket> listWorldSockets = new List<Socket>();


    // To communicate with players
    private static TcpListener tcpListener;
    // Text stream
    private static Dictionary<string, NetworkStream> playerNameStream = new Dictionary<string, NetworkStream>();
    // Buffers for the code
    private static Dictionary<string, byte[]> playerNameCode = new Dictionary<string, byte[]>();


    long matchid;
    public long MatchId { get => matchid; }

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;


        // Start
        SetupTcpListener();
        SetupWorldStreamServer();


        // Register in db
        matchid = ServerDatabaseConnection.Instance.RegisterGame(dataport, worldport);
    }

    // Update is called once per frame
    void Update()
    {

    }
     
    #region Data like inventory, code or names

    public void SetupTcpListener()
    {
        playerNameStream = new Dictionary<string, NetworkStream>();

        tcpListener = new TcpListener(new IPEndPoint(IPAddress.Any, dataport));
        tcpListener.Start();
        tcpListener.BeginAcceptTcpClient(new AsyncCallback(AcceptTCPClientsCallback), null);
    }

    private void AcceptTCPClientsCallback(IAsyncResult ar)
    {

        Debug.Log("[SERVER] Akzeptiere Verbindung");


        TcpClient client = tcpListener.EndAcceptTcpClient(ar);

        NetworkStream networkStream = client.GetStream();

        byte[] buffer = new byte[DATA_SIZE];

        networkStream.Read(buffer, 0, buffer.Length);
        // Namensverfügbarkeit überprüfen
        string name = System.Text.Encoding.UTF8.GetString(buffer);
        // Antworten
        if (gameManager.IsNameAvailable(name))
        {
            //Debug.Log("[SERVER] Name verfügbar");
            byte[] respondData = new byte[] { 1 };

            networkStream.Write(respondData, 0, 1);
            networkStream.Flush();

            //Debug.Log("[SERVER] Informieren abgeschlossen");

            name = name.Replace("\0", "");
            gameManager.RegisterPlayer(new Player(name));
            //Debug.Log("[SERVER] Spieler beim GameMAnager registriert");
            // Add player to Dictionary
            playerNameStream.Add(name, networkStream);

            // reuse the buffer for code
            playerNameCode.Add(name, buffer);


            // Debug.Log("[SERVER] Spieler "+name+" hinzugefügt");
        }
        else
        {
            // Debug.Log("[SERVER] Spieler " + name + " abgelehnt");
            byte[] respondData = new byte[] { 0 };

            networkStream.Write(respondData, 0, 1);
        }

        if (gameManager.AcceptNewPlayers)
        {
            Debug.Log("[SERVER] Warte auf weitere Spieler...");
            tcpListener.BeginAcceptTcpClient(new AsyncCallback(AcceptTCPClientsCallback), null);
        }
        else
        {
            Debug.Log("[SERVER] Starte Spiel...");
            // Inform db
            ServerDatabaseConnection.Instance.StartGame();
            gameManager.StartGame();
        }
    }

    // Send 1 to the player and the id of his robot game object (-> Can show it to the user)
    public void ActivatePlayerInput(Player player, int roboterid)
    {
        if (playerNameStream[player.Name] == null)
        {
            return;
        }

        NetworkStream stream = playerNameStream[player.Name];

        byte[] buffer = playerNameCode[player.Name];
        buffer[0] = 1;
        // Send robot go id with
        byte[] robotid = BitConverter.GetBytes(roboterid);
        for (int i = 0; i < robotid.Length; i++)
        {
            buffer[1 + i] = robotid[i];
        }
        Debug.Log("Sende id of GameObject ( " + roboterid + " ) an " + player.Name);
        stream.Write(buffer, 0, buffer.Length);

        StartReceivingCode(player.Name);
    }

    // Send 2 to the player
    public void InformAboutDeath(Player p)
    {
        NetworkStream stream = playerNameStream[p.Name];

        byte[] buffer = playerNameCode[p.Name];
        buffer[0] = 2;
        stream.Write(buffer, 0, buffer.Length);
    }

    // Sends 3 to the player
    public void InformAboutWin(Player player)
    {
        NetworkStream stream = playerNameStream[player.Name];

        byte[] buffer = playerNameCode[player.Name];
        buffer[0] = 3;
        stream.Write(buffer, 0, buffer.Length);

        Debug.Log("Gewinner");
    }


    // Transmit inventory to a player
    public void SendInventory(Player p)
    {
        Debug.Log("Sende inventory");
        NetworkStream stream = playerNameStream[p.Name];


        byte[] invBytes = Inventory.FromRobot(p.JKBRRobot).ToBytes();
        //Debug.Log(Inventory.FromRobot(p.JKBRRobot).ToString());
        //Debug.Log(Inventory.FromBytes(invBytes).ToString());
        stream.BeginWrite(invBytes, 0, invBytes.Length, null, null);
    }

    public void StartReceivingCode(string playername)
    {
        playerNameCode[playername] = new byte[DATA_SIZE];

        playerNameStream[playername].BeginRead(playerNameCode[playername], 0, DATA_SIZE, ReceiveCode, playername);
    }


    public void ReceiveCode(IAsyncResult ar)
    {
        string playername = (string)ar.AsyncState;

        string code = System.Text.Encoding.UTF8.GetString(playerNameCode[playername]);
        Debug.Log("[SERVER] Code: " + code);
        gameManager.ReceiveCode(playername, code);

        //  playerNameStream[playername].BeginWrite(new byte[]{ 1}, 0, 1, ReceiveCode, playername);
    }

    void SentCodeResult(IAsyncResult ar)
    {
    }
    #endregion

    #region Changes

    public void SetupWorldStreamServer()
    {
        Debug.Log("Setting up stream server...");
        _serverSocket.Bind(new IPEndPoint(IPAddress.Any, worldport));
        _serverSocket.Listen(MAX_CONNECTIONS);

        _serverSocket.BeginAccept(new System.AsyncCallback(AccepltWorldStreamCallback), null);

    }

    private static void AccepltWorldStreamCallback(IAsyncResult AR)
    {
        Socket client = _serverSocket.EndAccept(AR);

        listWorldSockets.Add(client);

        _serverSocket.BeginAccept(new AsyncCallback(AccepltWorldStreamCallback), null);
    }


    public void BroadcastChange(Change c)
    {

        // To all players
        foreach (var item in listWorldSockets)
        {
            byte[] bs = c.ToBytes();
            //  Debug.Log("[SERVER] Sende " + c.ToString());
            item.BeginSend(bs, 0, Change.BytesLength, SocketFlags.None, SentTest, null);

        }
    }

    private void SentTest(IAsyncResult ar)
    {
        // Debug.Log("[SERVER] Sendete WELT dingsllsdafjdölasfjölsaö");
    }
    #endregion

    private void OnDestroy()
    {
        _serverSocket.Close();
        tcpListener.Stop();
    }

}
