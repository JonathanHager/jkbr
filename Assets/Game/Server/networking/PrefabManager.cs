﻿using Assets.Server.game;
using javakarol;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject GetRobotPrefab(Player player)
    {
        return Resources.Load<GameObject>( ServerDatabaseConnection.Instance.RobotPrefabPath(player.Name));
    }

    public GameObject GetWeaponPrefab(string itemName)
    {
        var path = ServerDatabaseConnection.Instance.WeaponPrefabPath(itemName);
        Debug.Log("WeaponPrefab: " + path);
        return Resources.Load<GameObject>(path);
    }

    internal GameObject GetConsumablePrefab(string v)
    {
        var path = ServerDatabaseConnection.Instance.ConsumablePrefabPath(v);
        Debug.Log("ConsumablePrefab: " + path);
        return Resources.Load<GameObject>(ServerDatabaseConnection.Instance.ConsumablePrefabPath(v));
    }

    internal GameObject GetBuildItemPrefab(string v)
    {
        var path = ServerDatabaseConnection.Instance.BuildItemPrefabPath(v);
        Debug.Log("BuildItemPrefab: " + path);
        return Resources.Load<GameObject>(ServerDatabaseConnection.Instance.BuildItemPrefabPath(v));
    }
    
    public int GetPrefabIdOfWeapon(string name)
    {
        return (int)ServerDatabaseConnection.Instance.GetPrefabIdOfWeapon(name);
    }
    public int GetPrefabIdOfConsumable(string name)
    {
        return (int)ServerDatabaseConnection.Instance.GetPrefabIdOfConsumable(name);
    }
    public int GetPrefabIdOfBuildItem(string name)
    {
        return (int)ServerDatabaseConnection.Instance.GetPrefabIdOfBuildItem(name);
    }


    public GameObject GetBlockPrefab(string type)
    {
        return Resources.Load<GameObject>(ServerDatabaseConnection.Instance.GetBlockPrefabPath(type));
    }

    public int GetPrefabIdOfBlock(string type)
    {
        return (int)ServerDatabaseConnection.Instance.GetBlockPrefabId(type);
    }

    public int GetPrefabIdOfRobot(Player p)
    {
        return (int)ServerDatabaseConnection.Instance.RobotPrefabId(p.Name);
    }



}
