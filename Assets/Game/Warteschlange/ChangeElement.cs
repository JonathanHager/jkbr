﻿using Assets.Server.networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Server.Warteschlange
{
    class ChangeElement
    {

        ChangeElement next;

        public ChangeElement Next
        {
            set { next = value; }
            get { return next; }
        }

        Change change;

        public ChangeElement(Change c)
        {
            this.change = c;
        }


        public Change Change
        {
            get { return this.change; }
        }

    }
}
