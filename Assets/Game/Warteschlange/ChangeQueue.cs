﻿using Assets.Server.networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Server.Warteschlange
{
    class ChangeQueue
    {

        ChangeElement start, end;

        public ChangeQueue()
        {

        }


        public void AddChange(Change c)
        {
            ChangeElement ce = new ChangeElement(c);

            ce.Next = start;

            start = ce;

            if (end == null)
            {
                end = start;
            }
        }

        public Change GetFirst()
        {
            ChangeElement ce = start;

            start = start.Next;
            
            return ce.Change;
        }

        public bool HasNext
        {
            get { return start != null; }
        }
    }
}
