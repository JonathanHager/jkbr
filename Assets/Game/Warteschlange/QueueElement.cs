﻿using Assets.Server.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Warteschlange
{
    abstract class QueueElement
    {
        QueueElement next;

        public QueueElement Next
        {
            get { return next; }
        }


        

        public abstract void Execute(GameManager gm);

        public void SetNextQueueElement(QueueElement qe)
        {
                next = qe;
            
        }
    }
}
