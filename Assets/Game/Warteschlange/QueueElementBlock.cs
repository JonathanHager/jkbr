﻿using javakarol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Warteschlange
{
    class QueueElementBlock : QueueElement
    {

        int x, y;
        Block b;

        public QueueElementBlock(Block b, int x, int y)
        {
            this.b = b;
            this.x = x;
            this.y = y;
        }

        public override void Execute(GameManager gm)
        {
            gm.PlaceBlockGameObject(b, x, y);
        }
    }
}
