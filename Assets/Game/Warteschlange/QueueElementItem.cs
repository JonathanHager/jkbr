﻿using javakarol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Warteschlange
{
    class QueueElementItem : QueueElement
    {

        private Item item;
        private int x, y;

        public QueueElementItem(Item i, int x, int y)
        {
            this.item = i;
            this.x = x;
            this.y = y;
        }

        public override void Execute(GameManager gm)
        { gm.SpawnItemGameObject(item, x, y);
        }
    }
}
