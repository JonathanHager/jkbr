﻿using Assets.Server.game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Warteschlange
{
    class QueueElementPlayer : QueueElement
    {

        // Data
        Player item;



        public QueueElementPlayer(Player player)
        {
            item = player;
        }

        public override void Execute(GameManager gm)
        {
            GameObject go = gm.CreateRobotGameObject(item);
            int id = gm.gameObjectManager.RegisterGameObject(go, gm.prefabManager.GetPrefabIdOfRobot(item));
            item.RobotObjectID = id;
            gm.gameObjectManager.RotateGameObject(id, 0, 1, 0, gm.DegreesByOrientation(item.JKBRRobot.getFacing()));

            JKBRServer.Instance.ActivatePlayerInput(item, item.RobotObjectID) ;
        }
        
    }
}
