﻿using Assets.Server.game;
using javakarol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Warteschlange
{
    class Warteschlange
    {

        QueueElement start;
        QueueElement end;

        public Warteschlange()
        {

        }


        public void AddPlayerGameObject(Player p)
        {
            QueueElement n = new QueueElementPlayer(p);

            if (start == null)
            {
                start = n;
                end = n;
            }
            else
            {
                end.SetNextQueueElement(n);
                end = n;
            }
        }
        public void AddItemGameObject(Item i, int x, int y)
        {
            QueueElement n = new QueueElementItem(i, x, y);

            if (start == null)
            {
                start = n;
                end = n;
            }
            else
            {
                end.SetNextQueueElement(n);
                end = n;
            }
        }
        public void AddBlockGameObject(Block i, int x, int y)
        {
            QueueElement n = new QueueElementBlock(i, x, y);

            if (start == null)
            {
                start = n;
                end = n;
            }
            else
            {
                end.SetNextQueueElement(n);
                end = n;
            }
        }

        public QueueElement GetNextElement()
        {
            if(start != null)
            {
                QueueElement temp = start;
                start = start.Next;
                return temp;
            }
            return null;
        }

        public bool HasNext
        {
            get { return start != null; }
        }
    }
}
