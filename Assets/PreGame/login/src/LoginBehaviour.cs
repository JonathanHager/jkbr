﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginBehaviour : MonoBehaviour
{

    public InputField inputTextUsername;
    public InputField inputTextPassword;

    public Text errorText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TryToLogIn()
    {

      if(  DatabaseConnection.Instance.LogIn(inputTextUsername.text, inputTextPassword.text))
        {
            SwitchToLoggedInScene();
        }
        else
        {
            errorText.gameObject.SetActive(true) ;
        }
    }

    void SwitchToLoggedInScene()
    {
        SceneManager.LoadScene(1);
    }
}
