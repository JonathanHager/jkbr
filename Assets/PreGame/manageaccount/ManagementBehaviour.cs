﻿using Assets.manageaccount.src;
using Assets.PreGame.manageaccount.src;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagementBehaviour : MonoBehaviour
{

    public Text textUsername;
    public Text textIntCoins;


    // Skin View
    public GameObject SkinPanel;
    public GameObject ScrollViewContent;

    public GameObject SkinPrefab;
    int shownSkins = 0;


    // MAtches
    public GameObject MatchesPanel;


    // Start is called before the first frame update
    void Start()
    {

        textUsername.text = DatabaseConnection.Instance.ConnectedPlayer;
        textIntCoins.text = DatabaseConnection.Instance.GetIntCoins() + " IntCoins";
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region skins
    public void ShowSkins()
    {

        MatchesPanel.SetActive(false);
        SkinPanel.SetActive(true);


        if (shownSkins != 0)
        {
            foreach (Transform child in ScrollViewContent.transform)
            {
                Destroy(child.gameObject);
            }
            shownSkins = 0;
        }

        List<SkinInfo> ownedskins = DatabaseConnection.Instance.OwnedSkins();

        foreach (var item in ownedskins)
        {
            AddOwnedSkin(item);
        }

        List<SkinInfo> skins = DatabaseConnection.Instance.AllSkins();

        foreach (var item in skins)
        {
            if (!ownedskins.Contains(item))
                AddBuyableSkin(item);
        }

    }


    void AddOwnedSkin(SkinInfo info)
    {
        GameObject skingo = GameObject.Instantiate(SkinPrefab, ScrollViewContent.transform);

        skingo.transform.Translate(new Vector3(shownSkins * 200, 0));

        Image image = skingo.GetComponentInChildren<Image>();
        Text nametext = skingo.GetComponentInChildren<Text>();
        Button buybutton = skingo.GetComponentInChildren<Button>();
        Text pricetext = buybutton.GetComponentInChildren<Text>();

        image.sprite = Resources.Load<Sprite>(info.Imagepath);
        Debug.Log(info.Imagepath);
        nametext.text = info.Name;

        if (!info.Equals(DatabaseConnection.Instance.EquippedSkin))
        {
            pricetext.text = "Equip";
            buybutton.interactable = true;

            buybutton.onClick.AddListener(() => equipSkin(info));
        }


        shownSkins++;
    }

    void AddBuyableSkin(SkinInfo info)
    {

        GameObject skingo = GameObject.Instantiate(SkinPrefab, ScrollViewContent.transform);

        skingo.transform.Translate(new Vector3(shownSkins * 200, 0));

        Image image = skingo.GetComponentInChildren<Image>();
        Text nametext = skingo.GetComponentInChildren<Text>();
        Button buybutton = skingo.GetComponentInChildren<Button>();
        Text pricetext = buybutton.GetComponentInChildren<Text>();

        image.sprite = Resources.Load<Sprite>(info.Imagepath);
        nametext.text = info.Name;
        pricetext.text = (info.Rarity * 100) + " IntCoins";
        buybutton.interactable = true;

        buybutton.onClick.AddListener(() => buySkin(info, skingo));

        shownSkins++;
    }

    void buySkin(SkinInfo sk, GameObject skingo)
    {
        bool success = DatabaseConnection.Instance.BuySkinIfPossible(sk);
        if(success)
         textIntCoins.text = DatabaseConnection.Instance.GetIntCoins() + " IntCoins";
    }

    void equipSkin(SkinInfo info)
    {
        DatabaseConnection.Instance.EquipSkin(info);
        ShowSkins();
    }

    #endregion

    #region matchs

    public void ShowMatches()
    {
        SkinPanel.SetActive(false);
        MatchesPanel.SetActive(true);

    }

    #endregion


    #region Play

    public void FindGame()
    {
        ServerInfo server = DatabaseConnection.Instance.FindServer();

        if (server == null)
        {
            showNoServerFound();
        }
        else
        {
            SceneManager.LoadScene(2);
        }

    }

    void showNoServerFound()
    {
    }
    #endregion
}
