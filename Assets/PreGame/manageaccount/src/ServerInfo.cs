﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.PreGame.manageaccount.src
{
    public class ServerInfo
    {

        long id;
        string hostname;
        int dataport;
        int worldport;

        public ServerInfo(long id, string hostname, int dataport, int worldport)
        {
            this.id = id;
            this.hostname = hostname;
            this.dataport = dataport;
            this.worldport = worldport;
        }

        public long Id { get => id; }
        public string Hostname { get => hostname; }
        public int Dataport { get => dataport; }
        public int Worldport { get => worldport;  }
    }
}
