﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.manageaccount.src
{
   public class SkinInfo
    {
        long id;
        int rarity;
        string imagepath;
        string name;


        public SkinInfo(long id, int r, string name, string path)
        {
            this.id = id;
            this.rarity = r;
            this.name = name;
            this.imagepath = path;
        }

        public int Rarity { get => rarity; }
        public string Imagepath { get => imagepath;}
        public string Name { get => name;  }
        public long Id { get => id;}


        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            SkinInfo cmp = (SkinInfo)obj;
            return cmp.id == this.id;
        }

        // override object.GetHashCode
        public override int GetHashCode()
        { 
            return base.GetHashCode();
        }
    }
}
