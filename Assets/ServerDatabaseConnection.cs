﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using UnityNpgsql;
using Assets.manageaccount.src;
using Assets.PreGame.manageaccount.src;

public class ServerDatabaseConnection
{
    public static ServerDatabaseConnection Instance = new ServerDatabaseConnection();

    string conString = "Server=192.168.178.52;Port=5432;" +
            "Database=jkbr;" +
            "User ID=UnityServer;" +
            "Password=secret;";

    ServerInfo me;

    public long RegisterGame(int dataport, int worldport)
    {
        string hostname = Dns.GetHostName();

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            NpgsqlCommand insert = new NpgsqlCommand("INSERT INTO public.\"Server\" (hostname, portdata, portworld ) SELECT '" + hostname + "', " + dataport + ", " + worldport + " " +
                "WHERE NOT EXISTS( SELECT hostname, portdata, portworld FROM public.\"Server\" WHERE hostname = '"+hostname+"' and portdata = "+dataport+" and portworld = "+worldport+ " ); ", conn);
           // Debug.Log(insert.CommandText);
            insert.ExecuteNonQuery();

            insert.Dispose();


            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Server\".id FROM public.\"Server\" WHERE public.\"Server\".hostname = '" + hostname + "' and public.\"Server\".portdata = " + dataport + " and public.\"Server\".portworld = " + worldport, conn);
          //  Debug.Log(command.CommandText);

            var reader = command.ExecuteReader();
            reader.Read();
            long id = (long)reader[0];

            me = new ServerInfo(id, hostname, dataport, worldport);

            reader.Close();
            command.Dispose();


            NpgsqlCommand insertMatch = new NpgsqlCommand("INSERT INTO public.\"Match\" (server) VALUES (" + id + ")", conn);

            insertMatch.ExecuteNonQuery();

            insertMatch.Dispose();
            

            command = new NpgsqlCommand("SELECT public.\"Match\".id FROM public.\"Server\", public.\"Match\" WHERE public.\"Server\".id = public.\"Match\".server ORDER BY id DESC ", conn);
            //  Debug.Log(command.CommandText);

            reader = command.ExecuteReader();
            reader.Read();
            long matchid = (long)reader[0];
            reader.Close();
            command.Dispose();

            return matchid;
        }

    }

    public void StartGame()
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();

            NpgsqlCommand startgame = new NpgsqlCommand("UPDATE public.\"Match\" SET started = True WHERE started = False and server = " + me.Id, conn);

            startgame.ExecuteNonQuery();

            startgame.Dispose();


        }

    }


    public string RobotPrefabPath(string playername)
    {
        

        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
         //   Debug.Log("Connected " + conn.Database);
         //   Debug.Log("'" + playername + "'");
            string text = "SELECT public.\"Prefab\".path FROM public.\"Prefab\", public.\"RobotSkin\",public.\"PlayerInfo\" " +
                " WHERE public.\"Prefab\".id=public.\"RobotSkin\".prefab and public.\"PlayerInfo\".username = '" + playername.Trim() + "' and public.\"RobotSkin\".id=public.\"PlayerInfo\".equippedSkin ";
            NpgsqlCommand command = new NpgsqlCommand(text, conn);
          //  Debug.Log(command.CommandText);
        //    Debug.Log(text);
            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();
            string path = (string)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }

    }

    public long RobotPrefabId(string playername)
    {


        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
          //  Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".id FROM public.\"Prefab\", public.\"RobotSkin\", public.\"PlayerInfo\" " +
                " WHERE public.\"Prefab\".id = public.\"RobotSkin\".prefab and public.\"RobotSkin\".id = public.\"PlayerInfo\".equippedSkin " +
                "and public.\"PlayerInfo\".username = '" + playername + "'"
                , conn);

            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            long path = (long)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }

    }

    public string WeaponPrefabPath(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".path from public.\"Prefab\",public.\"Weapon\"  WHERE public.\"Prefab\".id = public.\"Weapon\".prefab and public.\"Weapon\".name= '" + weaponname + "'", conn);
            
            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            string path = (string)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }
    public string ConsumablePrefabPath(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
           // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".path from public.\"Prefab\",public.\"Consumable\"  WHERE public.\"Prefab\".id = public.\"Consumable\".prefab and public.\"Consumable\".name= '" + weaponname + "'", conn);

            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            string path = (string)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }
    public string BuildItemPrefabPath(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
          //  Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".path from public.\"Prefab\",public.\"BuildItem\"  WHERE public.\"Prefab\".id = public.\"BuildItem\".prefab and public.\"BuildItem\".name= '" + weaponname + "'", conn);
          //  Debug.Log(command.CommandText);
            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            string path = (string)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }


    #region ids
    public long GetPrefabIdOfWeapon(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
           // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".id from public.\"Prefab\",public.\"Weapon\"  WHERE public.\"Prefab\".id = public.\"Weapon\".prefab and public.\"Weapon\".name= '" + weaponname + "'", conn);

            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            long path = (long)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }
    public long GetPrefabIdOfConsumable(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
           // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".id from public.\"Prefab\",public.\"Consumable\"  WHERE public.\"Prefab\".id = public.\"Consumable\".prefab and public.\"Consumable\".name= '" + weaponname + "'", conn);

            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            long path = (long)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }

    public long GetPrefabIdOfBuildItem(string weaponname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
           // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".id from public.\"Prefab\",public.\"BuildItem\"  WHERE public.\"Prefab\".id = public.\"BuildItem\".prefab and public.\"BuildItem\".name= '" + weaponname + "'", conn);

            command.ExecuteNonQuery();
            var reader = command.ExecuteReader();
            reader.Read();

            long path = (long)reader[0];

            reader.Close();
            command.Dispose();

            return path;
        }
    }
    #endregion

    #region Blocks

    public string GetBlockPrefabPath(string blockname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            //Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".path FROM public.\"Prefab\", public.\"Block\"" +
                " WHERE public.\"Prefab\".id = pubic.\"Block\".prefab and public.\"Block\".type='" + blockname + "'"
                , conn);
            command.Connection = conn;
         //   Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (string)reader[0];
            }
            catch (System.Exception e)
            {
                Debug.LogError("Exception: Database refused connection: " + e.Message);
                return null;
            }
        }

    }
    public long GetBlockPrefabId(string blockname)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
           // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("SELECT public.\"Prefab\".id FROM public.\"Prefab\", public.\"Block\"" +
                " WHERE public.\"Prefab\".id = pubic.\"Block\".prefab and public.\"Block\".type='" + blockname + "'"
                , conn);
            command.Connection = conn;
           // Debug.Log(command.CommandText);
            try
            {
                var reader = command.ExecuteReader();
                reader.Read();

                return (long)reader[0];
            }
            catch (System.Exception e)
            {
                Debug.LogError("Exception: Database refused connection: " + e.Message);
                return -1;
            }
        }

    }
    #endregion

    #region match res

    public void SaveMatchResult(long matchid, string winner, int roundcount, List<string> players)
    {
        using (NpgsqlConnection conn = new NpgsqlConnection(conString))
        {
            conn.Open();
            // Debug.Log("Connected " + conn.Database);

            NpgsqlCommand command = new NpgsqlCommand("UPDATE public.\"Match\" SET winner = '" + winner + "', roundcount = " + roundcount + " WHERE public.\"Match\".id = " + matchid
                , conn);

            command.ExecuteNonQuery();
            command.Dispose();

            foreach (string player in players)
            {
                command = new NpgsqlCommand("INSERT INTO public.\"player_played_match\" (player, match) VALUES ('"+player+"', "+matchid+")"
                , conn);
                command.ExecuteNonQuery();
                command.Dispose();
            }

        }

    }

    #endregion
}
